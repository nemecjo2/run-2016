#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=SATSolver
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/ClassHeap.o \
	${OBJECTDIR}/DataTypes.o \
	${OBJECTDIR}/FileManager.o \
	${OBJECTDIR}/Frame.o \
	${OBJECTDIR}/FrameStack.o \
	${OBJECTDIR}/GarbageCollector.o \
	${OBJECTDIR}/JavaClass.o \
	${OBJECTDIR}/JavaClassPrinter.o \
	${OBJECTDIR}/ObjectHeap.o \
	${OBJECTDIR}/RuntimeEnvironment.o \
	${OBJECTDIR}/TestEnv/TestEnv.o \
	${OBJECTDIR}/main.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f4 \
	${TESTDIR}/TestFiles/f6 \
	${TESTDIR}/TestFiles/f5 \
	${TESTDIR}/TestFiles/f10 \
	${TESTDIR}/TestFiles/f9 \
	${TESTDIR}/TestFiles/f1 \
	${TESTDIR}/TestFiles/f7 \
	${TESTDIR}/TestFiles/f2 \
	${TESTDIR}/TestFiles/f3 \
	${TESTDIR}/TestFiles/f8

# Test Object Files
TESTOBJECTFILES= \
	${TESTDIR}/tests/ArrayTest.o \
	${TESTDIR}/tests/ClassHeapTest.o \
	${TESTDIR}/tests/ControlStructures.o \
	${TESTDIR}/tests/GarbageCollectorTest.o \
	${TESTDIR}/tests/InheritanceTest.o \
	${TESTDIR}/tests/JavaClassTest.o \
	${TESTDIR}/tests/MethodsTest.o \
	${TESTDIR}/tests/RuntimeTest.o \
	${TESTDIR}/tests/SimpleArithmeticTest.o \
	${TESTDIR}/tests/Static.o

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-pedantic
CXXFLAGS=-pedantic

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/run

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/run: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/run ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/ClassHeap.o: ClassHeap.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ClassHeap.o ClassHeap.cpp

${OBJECTDIR}/DataTypes.o: DataTypes.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes.o DataTypes.cpp

${OBJECTDIR}/FileManager.o: FileManager.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FileManager.o FileManager.cpp

${OBJECTDIR}/Frame.o: Frame.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Frame.o Frame.cpp

${OBJECTDIR}/FrameStack.o: FrameStack.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FrameStack.o FrameStack.cpp

${OBJECTDIR}/GarbageCollector.o: GarbageCollector.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GarbageCollector.o GarbageCollector.cpp

${OBJECTDIR}/JavaClass.o: JavaClass.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/JavaClass.o JavaClass.cpp

${OBJECTDIR}/JavaClassPrinter.o: JavaClassPrinter.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/JavaClassPrinter.o JavaClassPrinter.cpp

${OBJECTDIR}/ObjectHeap.o: ObjectHeap.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ObjectHeap.o ObjectHeap.cpp

${OBJECTDIR}/RuntimeEnvironment.o: RuntimeEnvironment.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RuntimeEnvironment.o RuntimeEnvironment.cpp

${OBJECTDIR}/TestEnv/TestEnv.o: TestEnv/TestEnv.cpp
	${MKDIR} -p ${OBJECTDIR}/TestEnv
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TestEnv/TestEnv.o TestEnv/TestEnv.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-tests-subprojects .build-conf ${TESTFILES}
.build-tests-subprojects:

${TESTDIR}/TestFiles/f4: ${TESTDIR}/tests/ArrayTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f4 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f6: ${TESTDIR}/tests/ClassHeapTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f6 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f5: ${TESTDIR}/tests/ControlStructures.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f5 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f10: ${TESTDIR}/tests/GarbageCollectorTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f10 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f9: ${TESTDIR}/tests/InheritanceTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f9 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f1: ${TESTDIR}/tests/JavaClassTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f7: ${TESTDIR}/tests/MethodsTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f7 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f2: ${TESTDIR}/tests/RuntimeTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f2 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f3: ${TESTDIR}/tests/SimpleArithmeticTest.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3 $^ ${LDLIBSOPTIONS}   

${TESTDIR}/TestFiles/f8: ${TESTDIR}/tests/Static.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f8 $^ ${LDLIBSOPTIONS}   


${TESTDIR}/tests/ArrayTest.o: tests/ArrayTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/ArrayTest.o tests/ArrayTest.cpp


${TESTDIR}/tests/ClassHeapTest.o: tests/ClassHeapTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/ClassHeapTest.o tests/ClassHeapTest.cpp


${TESTDIR}/tests/ControlStructures.o: tests/ControlStructures.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/ControlStructures.o tests/ControlStructures.cpp


${TESTDIR}/tests/GarbageCollectorTest.o: tests/GarbageCollectorTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/GarbageCollectorTest.o tests/GarbageCollectorTest.cpp


${TESTDIR}/tests/InheritanceTest.o: tests/InheritanceTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/InheritanceTest.o tests/InheritanceTest.cpp


${TESTDIR}/tests/JavaClassTest.o: tests/JavaClassTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/JavaClassTest.o tests/JavaClassTest.cpp


${TESTDIR}/tests/MethodsTest.o: tests/MethodsTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/MethodsTest.o tests/MethodsTest.cpp


${TESTDIR}/tests/RuntimeTest.o: tests/RuntimeTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/RuntimeTest.o tests/RuntimeTest.cpp


${TESTDIR}/tests/SimpleArithmeticTest.o: tests/SimpleArithmeticTest.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/SimpleArithmeticTest.o tests/SimpleArithmeticTest.cpp


${TESTDIR}/tests/Static.o: tests/Static.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I. -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/Static.o tests/Static.cpp


${OBJECTDIR}/ClassHeap_nomain.o: ${OBJECTDIR}/ClassHeap.o ClassHeap.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ClassHeap.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ClassHeap_nomain.o ClassHeap.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ClassHeap.o ${OBJECTDIR}/ClassHeap_nomain.o;\
	fi

${OBJECTDIR}/DataTypes_nomain.o: ${OBJECTDIR}/DataTypes.o DataTypes.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/DataTypes.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DataTypes_nomain.o DataTypes.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/DataTypes.o ${OBJECTDIR}/DataTypes_nomain.o;\
	fi

${OBJECTDIR}/FileManager_nomain.o: ${OBJECTDIR}/FileManager.o FileManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/FileManager.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FileManager_nomain.o FileManager.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/FileManager.o ${OBJECTDIR}/FileManager_nomain.o;\
	fi

${OBJECTDIR}/Frame_nomain.o: ${OBJECTDIR}/Frame.o Frame.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Frame.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Frame_nomain.o Frame.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Frame.o ${OBJECTDIR}/Frame_nomain.o;\
	fi

${OBJECTDIR}/FrameStack_nomain.o: ${OBJECTDIR}/FrameStack.o FrameStack.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/FrameStack.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FrameStack_nomain.o FrameStack.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/FrameStack.o ${OBJECTDIR}/FrameStack_nomain.o;\
	fi

${OBJECTDIR}/GarbageCollector_nomain.o: ${OBJECTDIR}/GarbageCollector.o GarbageCollector.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/GarbageCollector.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GarbageCollector_nomain.o GarbageCollector.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/GarbageCollector.o ${OBJECTDIR}/GarbageCollector_nomain.o;\
	fi

${OBJECTDIR}/JavaClass_nomain.o: ${OBJECTDIR}/JavaClass.o JavaClass.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/JavaClass.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/JavaClass_nomain.o JavaClass.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/JavaClass.o ${OBJECTDIR}/JavaClass_nomain.o;\
	fi

${OBJECTDIR}/JavaClassPrinter_nomain.o: ${OBJECTDIR}/JavaClassPrinter.o JavaClassPrinter.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/JavaClassPrinter.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/JavaClassPrinter_nomain.o JavaClassPrinter.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/JavaClassPrinter.o ${OBJECTDIR}/JavaClassPrinter_nomain.o;\
	fi

${OBJECTDIR}/ObjectHeap_nomain.o: ${OBJECTDIR}/ObjectHeap.o ObjectHeap.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ObjectHeap.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ObjectHeap_nomain.o ObjectHeap.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ObjectHeap.o ${OBJECTDIR}/ObjectHeap_nomain.o;\
	fi

${OBJECTDIR}/RuntimeEnvironment_nomain.o: ${OBJECTDIR}/RuntimeEnvironment.o RuntimeEnvironment.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/RuntimeEnvironment.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RuntimeEnvironment_nomain.o RuntimeEnvironment.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/RuntimeEnvironment.o ${OBJECTDIR}/RuntimeEnvironment_nomain.o;\
	fi

${OBJECTDIR}/TestEnv/TestEnv_nomain.o: ${OBJECTDIR}/TestEnv/TestEnv.o TestEnv/TestEnv.cpp 
	${MKDIR} -p ${OBJECTDIR}/TestEnv
	@NMOUTPUT=`${NM} ${OBJECTDIR}/TestEnv/TestEnv.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TestEnv/TestEnv_nomain.o TestEnv/TestEnv.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/TestEnv/TestEnv.o ${OBJECTDIR}/TestEnv/TestEnv_nomain.o;\
	fi

${OBJECTDIR}/main_nomain.o: ${OBJECTDIR}/main.o main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Wall -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main_nomain.o main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/main.o ${OBJECTDIR}/main_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f4 || true; \
	    ${TESTDIR}/TestFiles/f6 || true; \
	    ${TESTDIR}/TestFiles/f5 || true; \
	    ${TESTDIR}/TestFiles/f10 || true; \
	    ${TESTDIR}/TestFiles/f9 || true; \
	    ${TESTDIR}/TestFiles/f1 || true; \
	    ${TESTDIR}/TestFiles/f7 || true; \
	    ${TESTDIR}/TestFiles/f2 || true; \
	    ${TESTDIR}/TestFiles/f3 || true; \
	    ${TESTDIR}/TestFiles/f8 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
