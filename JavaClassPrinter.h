#ifndef JAVACLASSPRINTER_H
#define JAVACLASSPRINTER_H

/**
 * Class for debug print methods of JavaClass.
 */
class JavaClassPrinter {
    public:
        void printJavaClass();
        void printConstantPoolItem(int i);
        void printFieldItem(FieldItem *);
        void printMethodItem(MethodItem *);
        void printAttributeItem(AttributeItem *);
        void printClassAccessFlags(u2);
        void printMethodAccessFlags(u2);

        JavaClass* javaclass;
};


#endif