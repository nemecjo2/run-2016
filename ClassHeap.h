#ifndef CLASSHEAP_H
#define CLASSHEAP_H

#include "JavaClass.h"

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

using namespace std;

class ObjectHeap;

/**
 * Class used for maintenance of class definition objects.
 */
class ClassHeap {
    public:

        /**
         * ClassHeap constructor.
         * @param classes_path Path to folder where .class files are stored.
         */
         ClassHeap(const char*);
        ~ClassHeap();

        /**
         * Adds JavaClass to ClassHeap.
         * @param javaclass JavaClass object to be added.
         * @return bool True if JavaClass was successfully added, else otherwise.
         */
        bool addClass(JavaClass*);

        /**
         * Gets class from Class map.
         * @param class_name Class name.
         * @return Pointer to JavaClass.
         */
        JavaClass* getClass(string);

        /**
         * Gets iterator-begin and iterator-end.
         * @param iterator-begin.
         * @param iterator-end.
         */
        void getIterator(map <string, JavaClass*>::iterator&, map <string, JavaClass*>::iterator&);

        ObjectHeap* object_heap;

    private:
        map <string, JavaClass*> class_map;
        const char* classes_path;

        /**
         * Loads class from Java classfile.
         * @param class_name_string Class name.
         * @return Pointer to JavaClass.
         * @throw runtime_error Throws if classfile does not exist.
         */
        JavaClass* loadClass(string);
};

#endif