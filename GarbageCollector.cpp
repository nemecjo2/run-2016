#include <cstring>

#include "GarbageCollector.h"

GarbageCollector::GarbageCollector(u4 heap_size, Variable* memory, bool* memory_usages, FrameStack* frame_stack, ClassHeap* class_heap, FileManager* open_files) :
    heap_size(heap_size),
    memory(memory),
    memory_usages(memory_usages),
    frame_stack(frame_stack),
    class_heap(class_heap),
    open_files(open_files) {
    memory_marks = new bool[heap_size];
}

GarbageCollector::~GarbageCollector() {
    delete [] memory_marks;
}

///////////////////////////////////////////////////////
// Public methods.
void GarbageCollector::collectGarbage() {
    memset(memory_marks, false, heap_size);
    mark();
    sweep();
}

///////////////////////////////////////////////////////
// Private methods.
void GarbageCollector::mark() {
    Frame* actual;

    for (u4 i = 0; i < frame_stack->size(); ++i) {
        actual = frame_stack->getFrameAt(i);

        for (unsigned int j = 0; j < actual->getLocalVariablesSize(); ++j)
            markObjectHandler(actual->getLocalVariable(j));

        for (unsigned int j = 0; j < actual->getStackSize(); ++j)
            markObjectHandler(actual->getStackItem(j));
    }

    JavaClass* actual_class;
    map <string, JavaClass*>::iterator iterator;
    map <string, JavaClass*>::iterator end;
    class_heap->getIterator(iterator, end);
    for (; iterator != end; ++iterator) {
        actual_class = iterator->second;

        for (u2 i = 0; i < actual_class->fields_count; ++i)
            markObjectHandler(actual_class->fields[i]->value);
    }
}

void GarbageCollector::markObjectHandler(ObjectHandler* object_handler) {
    if (object_handler == NULL)
       return;

    u4 index = object_handler->heap_ptr - memory;
    memory_marks[index] = true;

    u1 type = object_handler->type;
    if (type == FIELD_CLASS) {
        s4 fields_count = object_handler->heap_ptr->_int;
        if(fields_count != object_handler->class_definition->fields_count)
            throw runtime_error("Garbage collector found corrupted object handler.\n");
        FieldItem* field_item;

        char * className;
        object_handler->class_definition->getName(className);

        for (s4 i = 0; i < fields_count; ++i) {
            memory_marks[index + 1 + i] = true;

            field_item = object_handler->class_definition->fields[i];
            char* descriptor;
            field_item->owner_class->getStringFromConstantPool(field_item->descriptor_index, descriptor);

            if (descriptor[0] == FIELD_CLASS || descriptor[0] == FIELD_ARRAY)
                markObjectHandler((ObjectHandler*) memory[index + 1 + i]._ptr);
        }
    }
    else if (type == FIELD_ARRAY) {
        s4 array_size = object_handler->heap_ptr->_int;

        memory_marks[index + 1] = true;
        u1 type = (object_handler->heap_ptr+1)->_char;
        if (type == FIELD_CLASS) {
            for (s4 i = 0; i < array_size; ++i) {
                memory_marks[index + 2 + i] = true;
                markObjectHandler((ObjectHandler*)((object_handler->heap_ptr + 2 + i)->_ptr));
            }
        } else {
            for (s4 i = 0; i < array_size; ++i)
                memory_marks[index + 2 + i] = true;
        }
    }
}

void GarbageCollector::sweep() {
    for (u4 i = 0; i < heap_size; ++i) {
        if (memory_usages[i] == false)
            continue;

        if (memory_marks[i] == true)
            memory_marks[i] = false;
        else {
            memory_usages[i] = false;
            memory[i]._int = 0;
        }
    }
}