#include "DataTypes.h"

// Reading methods
u1 readu1(char* &reader) {
    u1 value = __getu1(reader);
    ++reader;

    return value;
}

u2 readu2(char* &reader) {
    u2 value = __getu2(reader);
    reader += 2;

    return value;
}

u4 readu4(char* &reader) {
    u4 value = __getu4(reader);
    reader += 4;

    return value;
}

u1 looku1(char* reader) {
    return __getu1(reader);
}

u1 looku1(u1* reader) {
    return __getu1(reader);
}

u2 looku2(char* reader) {
    return __getu2(reader);
}

u2 looku2(u1* reader) {
    return __getu2(reader);
}

u4 looku4(char* reader) {
    return __getu4(reader);
}