#ifndef JAVACLASS_H
#define JAVACLASS_H

#include <cstdio>
#include <string>

#include "Constants.h"
#include "DataTypes.h"

using namespace std;

class ClassHeap;
class ObjectHeap;

/**
 * Class used for parsing JavaClass from Classfile.
 */
class JavaClass {
    public:

        /**
         * Create new JavaClass and add pointer to ClassHeap.
         * @param class_heap Pointer to ClassHeap.
         */
        JavaClass(ClassHeap*);

        /**
         * Parses JavaClass from given Classfile.
         * @param classfile FILE pointer to Classfile.
         * @return bool True if file is valid, false otherwise.
         */
        bool fetch(FILE*);

        /**
         * Loads specific constant pool item from given pointer by item tag.
         * @param reader Pointer to place for reading.
         * @return ConstantPoolItem* Pointer to created Item.
         */
        ConstantPoolItem* loadConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return GeneralConstantPoolItem* Pointer to created Item.
         */
        GeneralConstantPoolItem* loadGeneralConstantPoolItem(char*&, u1);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return ClassConstantPoolItem* Pointer to created Item.
         */
        ClassConstantPoolItem* loadClassConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return FieldrefConstantPoolItem* Pointer to created Item.
         */
        FieldrefConstantPoolItem* loadFieldrefConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return MethodrefConstantPoolItem* Pointer to created Item.
         */
        MethodrefConstantPoolItem* loadMethodrefConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return InterfaceMethodrefConstantPoolItem* Pointer to created Item.
         */
        InterfaceMethodrefConstantPoolItem* loadInterfaceMethodrefConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return StringConstantPoolItem* Pointer to created Item.
         */
        StringConstantPoolItem* loadStringConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return IntegerConstantPoolItem* Pointer to created Item.
         */
        IntegerConstantPoolItem* loadIntegerConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return FloatConstantPoolItem* Pointer to created Item.
         */
        FloatConstantPoolItem* loadFloatConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return LongConstantPoolItem* Pointer to created Item.
         */
        LongConstantPoolItem* loadLongConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return DoubleConstantPoolItem* Pointer to created Item.
         */
        DoubleConstantPoolItem* loadDoubleConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return NameAndTypeConstantPoolItem* Pointer to created Item.
         */
        NameAndTypeConstantPoolItem* loadNameAndTypeConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return Utf8ConstantPoolItem* Pointer to created Item.
         */
        Utf8ConstantPoolItem* loadUtf8ConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return MethodHandleConstantPoolItem* Pointer to created Item.
         */
        MethodHandleConstantPoolItem* loadMethodHandleConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return MethodTypeConstantPoolItem* Pointer to created Item.
         */
        MethodTypeConstantPoolItem* loadMethodTypeConstantPoolItem(char*&);

        /**
         * Loads constant pool item from given pointer.
         * @param reader Pointer to place for reading.
         * @return InvokeDynamicConstantPoolItem* Pointer to created Item.
         */
        InvokeDynamicConstantPoolItem* loadInvokeDynamicConstantPoolItem(char*&);

        /**
         * Loads field item from given pointer.
         * @param reader Pointer to place for reading.
         * @return FieldItem* Pointer to created Item.
         */
        FieldItem* loadFieldItem(char*&);

        /**
         * Loads method item from given pointer.
         * @param reader Pointer to place for reading.
         * @return MethodItem* Pointer to created Item.
         */
        MethodItem* loadMethodItem(char*&);

        /**
         * Loads specific attribute item from given pointer by Utf8 constant in CP.
         * @param reader Pointer to place for reading.
         * @return AttributeItem* Pointer to created Item.
         */
        AttributeItem* loadAttributeItem(char*&);

        /**
         * Loads general attribute item from given pointer by Utf8 constant in CP.
         * @param reader Pointer to place for reading.
         * @param name_index Attribute name index.
         * @param length Attribute length.
         * @return AttributeItem* Pointer to created Item.
         */
        AttributeItem* loadGeneralAttributeItem(char*&, u2, u4);

        /**
         * Loads constant value attribute item from given pointer.
         * @param reader Pointer to place for reading.
         * @return ConstantValueAttributeItem* Pointer to created Item.
         */
        ConstantValueAttributeItem* loadConstantValueAttributeItem(char*&, u2, u4);

        /**
         * Loads code attribute item from given pointer.
         * @param reader Pointer to place for reading.
         * @return CodeAttributeItem* Pointer to created Item.
         */
        CodeAttributeItem* loadCodeAttributeItem(char*&, u2, u4);

        /**
         * Loads exception item from given pointer.
         * @param reader Pointer to place for reading.
         * @return ExceptionItem* Pointer to created Item.
         */
        ExceptionItem* loadExceptionItem(char*&);

        /**
         * Gets constant pool item size for given item.
         * @param item Pointer to ConstantPoolItem.
         * @return int Size of given item.
         */
        int getConstantPoolItemSize(ConstantPoolItem*);

        /**
         * Gets Utf8 string from constant pool.
         * @param i Index to Constant pool.
         * @param output Pointer to output string for writing into it.
         * @return bool True if Utf8 string is available, false otherwise.
         */
        bool getStringFromConstantPool(u2, char*&);

        /**
         * Get Parameters count of Method by its descriptor.
         * @param i Index to Constant pool where descriptor of method is located.
         * @return int Method parameters count.
         */
        int getMethodParametersCount(u2);

        /**
         * Gets JavaClass name as Utf8 string.
         * @param output Pointer to output string for writing into it.
         */
        void getName(char*&);

        /**
         * Gets JavaClass name as string.
         * @return string JavaClass name.
         */
        string getName();

        /**
         * Gets parent JavaClass name as string.
         * @return string Parent JavaClass name.
         */
        string getParentName();

        /**
         * Has Class parent?
         * @return bool True if Class has parent, false otherwise.
         */
        bool hasParent();

        bool hasParent(JavaClass*);

        /**
         * Return parent Class of actual Class if exists.
         * @return JavaClass* pointer if Class has parent, NULL otherwise.
         */
        JavaClass* getParent();

        /**
         * Has Class specified method.
         * @param name Method name.
         * @return bool True if Class has specified method, false otherwise.
         */
        bool hasMethod(const char*);

        /**
         * Get Method from JavaClass by name.
         * @param name Method name.
         * @return MethodItem* Pointer to Method.
         * @throw runtime_error Throws if method does not exist.
         */
        MethodItem* getMethod(const char*);

        /**
         * Get Method from JavaClass by name.
         * @param name Method name.
         * @return MethodItem* Pointer to Method.
         * @throw runtime_error Throws if method does not exist.
         */
        MethodItem* getMethod(const char*, const char*);

        /**
         * Get Method from JavaClass by name.
         * @param name Method name.
         * @return MethodItem* Pointer to Method.
         * @throw runtime_error Throws if method does not exist.
         */
        MethodItem* getLocalMethod(const char*, const char*);

        /**
         * Get Method from JavaClass by name.
         * @param name Method name.
         * @return MethodItem* Pointer to Method.
         * @throw runtime_error Throws if method does not exist.
         */
        MethodItem* getMethod(const char*, const char*, JavaClass*, bool);
        
        /**
         * Get Class definition for method from JavaClass by name.
         * @param name Method name.
         * @return JavaClass* Pointer to Class definition.
         * @throw runtime_error Throws if method does not exist.
         */
        JavaClass* getClassForMethod(const char*);

        /**
         * Get Field index from JavaClass by name.
         * @param name Field name.
         * @return index Index of requested field in fields array.
         */
        u2 getFieldIndex(const char *);

        u4 magic;
        u2 minor_version;
        u2 major_version;
        u2 constant_pool_count;
        ConstantPoolItem** constant_pool; //[constant_pool_count-1];
        u2 access_flags;
        u2 this_class;
        u2 super_class;
        u2 interfaces_count;
        u2* interfaces; //[interfaces_count];
        u2 fields_count;
        u2 public_protected_fields_count;
        FieldItem** fields; //[fields_count];
        u2 methods_count;
        MethodItem** methods; //[methods_count];
        u2 attributes_count;
        AttributeItem** attributes; //[attributes_count];

        bool initialized;

        ClassHeap* class_heap;
        ObjectHeap* object_heap;

    private:
        u2 getInheritedFieldsCount();
        void getInheritedFields();
};

#endif