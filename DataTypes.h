#ifndef DATATYPES_H
#define DATATYPES_H

#include <stdint.h>
#include "Constants.h"

class ObjectHandler;

class JavaClass;

#define __getu4(p) (u4)( (u4)((p)[0])<<24 & 0xFF000000 | (u4)((p)[1])<<16 & 0x00FF0000 | (u4)((p)[2])<<8 & 0x0000FF00| (u4)((p)[3]) & 0x000000FF)
#define __getu2(p) (u2)((p)[0]<< 8 & 0x0000FF00 |(p)[1])
#define __getu1(p) (u1)((p)[0])

#define __geti4(p) (i4)( (u4)((p)[0])<<24 | (u4)((p)[1])<<16 | (u4)((p)[2])<<8 | (u4)((p)[3]) )
#define __geti2(p) (i2)(((p)[0]<<8)|(p)[1])

typedef unsigned char u1;
typedef unsigned short u2;
typedef uint32_t u4;

typedef signed char s1;
typedef signed short s2;
typedef int32_t s4;

/**
 * Reads u1 from reader and moves reader by 1 byte.
 * @param reader Pointer to place for reading.
 * @return u1 value
 */
u1 readu1(char* &reader);

/**
 * Reads u2 from reader and moves reader by 2 byte.
 * @param reader Pointer to place for reading.
 * @return u2 value
 */
u2 readu2(char* &reader);

/**
 * Reads u4 from reader and moves reader by 4 byte.
 * @param reader Pointer to place for reading.
 * @return u4 value
 */
u4 readu4(char* &reader);

/**
 * Reads u1 from reader. Does NOT move reader!
 * @param reader Pointer to place for reading.
 * @return u1 value
 */
u1 looku1(char* reader);

/**
 * Reads u1 from reader. Does NOT move reader!
 * @param reader Pointer to place for reading.
 * @return u1 value
 */
u1 looku1(u1* reader);

/**
 * Reads u2 from reader. Does NOT move reader!
 * @param reader Pointer to place for reading.
 * @return u2 value
 */
u2 looku2(char* reader);

/**
 * Reads u2 from reader. Does NOT move reader!
 * @param reader Pointer to place for reading.
 * @return u2 value
 */
u2 looku2(u1* reader);

/**
 * Reads u4 from reader. Does NOT move reader!
 * @param reader Pointer to place for reading.
 * @return u4 value
 */
u4 looku4(char* reader);

struct ExceptionItem {
    u2 start_pc;
    u2 end_pc;
    u2 handler_pc;
    u2 catch_type;

    int size;
};

///////////////////////////////////////////////////////
// Constants
class ConstantPoolItem {
    public:
        u1 tag;
};

class GeneralConstantPoolItem : public ConstantPoolItem { // # 4.4
    public:
        u1* content;
};

class ClassConstantPoolItem : public ConstantPoolItem { // # 4.4.1
    public:
        u2 name_index;
};

class FieldrefConstantPoolItem : public ConstantPoolItem { // # 4.4.2
    public:
        u2 class_index;
        u2 name_and_type_index;
};

class MethodrefConstantPoolItem : public ConstantPoolItem { // # 4.4.2
    public:
        u2 class_index;
        u2 name_and_type_index;
};

class InterfaceMethodrefConstantPoolItem : public ConstantPoolItem { // # 4.4.2
    public:
        u2 class_index;
        u2 name_and_type_index;
};

class StringConstantPoolItem : public ConstantPoolItem { // # 4.4.3
    public:
        u2 string_index;
};

class IntegerConstantPoolItem : public ConstantPoolItem { // # 4.4.4
    public:
        u4 bytes;
};

class FloatConstantPoolItem : public ConstantPoolItem { // # 4.4.4
    public:
        u4 bytes;
};

class LongConstantPoolItem : public ConstantPoolItem { // # 4.4.5
    public:
        u4 high_bytes;
        u4 low_bytes;
};

class DoubleConstantPoolItem : public ConstantPoolItem { // # 4.4.5
    public:
        u4 high_bytes;
        u4 low_bytes;
};

class NameAndTypeConstantPoolItem : public ConstantPoolItem { // # 4.4.6
    public:
        u2 name_index;
        u2 descriptor_index;
};

class Utf8ConstantPoolItem : public ConstantPoolItem { // # 4.4.7
    public:
        u2 length;
        u1* bytes; // [length]
};

class MethodHandleConstantPoolItem : public ConstantPoolItem { // # 4.4.8
    public:
        u1 reference_kind;
        u2 reference_index;
};

class MethodTypeConstantPoolItem : public ConstantPoolItem { // # 4.4.9
    public:
        u2 descriptor_index;
};

class InvokeDynamicConstantPoolItem : public ConstantPoolItem { // # 4.4.10
    public:
        u2 bootstrap_method_attr_index;
        u2 name_and_type_index;
};

///////////////////////////////////////////////////////
// Attributes
class AttributeItem {
    public:
        u2 name_index;
        u4 length;

        u2 type;
};

class GeneralAttributeItem : public AttributeItem { // # 4.7
    public:
        u1* info; // [attributeLength]

};

class ConstantValueAttributeItem : public AttributeItem { // #4.7.2
    public:
        u2 constantvalue_index;
};

class CodeAttributeItem : public AttributeItem { // #4.7.3
    public:
        u2 max_stack;
        u2 max_locals;
        u4 code_length;
        u1* code; // [code_length]
        u2 exception_table_length;
        ExceptionItem** exception_table; // [exception_table_length]
        u2 attributes_count;
        AttributeItem** attributes; // [attributes_count]
};

struct FieldItem {
    u2 flags;
    u2 name_index;
    u2 descriptor_index;
    u2 attributes_count;
    AttributeItem** attributes; // [attributesCount]
    ConstantValueAttributeItem* constant_value;
    ObjectHandler* value;
    JavaClass* owner_class;
};

struct MethodItem {
    u2 flags;
    u2 name_index;
    u2 descriptor_index;
    u2 attributes_count;
    AttributeItem** attributes; // [attributesCount]
    CodeAttributeItem* method_code;
    u4 method_parameters_cnt;
    JavaClass* owner_class;
};

union Variable {
    u1 _char;
    u2 _short;
    s4 _int;
    //f4 _float;
    void* _ptr;
};

struct ObjectHandler {
    u1 type;
    Variable* heap_ptr;
    JavaClass* class_definition;
};

#endif