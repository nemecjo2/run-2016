#include "Frame.h"

Frame::Frame(MethodItem* method, ObjectHandler* class_instance) :
    pc(0),
    method(method),
    local_variables(method->method_code->max_locals) {
    if (method->method_code->max_locals == 0)
        local_variables.resize(1);

    local_variables[0] = class_instance;
}

Frame::~Frame() {
}

u1 Frame::getNext() {
    return method->method_code->code[pc++];
}

ObjectHandler* Frame::popFromStack() {
    if (local_stack.empty()) {
        throw runtime_error("Stack is empty.");
    }
    ObjectHandler* ret = local_stack.top();
    local_stack.pop();
    return ret;
}

void Frame::pushOnStack(ObjectHandler* operand) {
    local_stack.push(operand);
}

ObjectHandler* Frame::getLocalVariable(int index) {
    return local_variables[index];
}

void Frame::setLocalVariable(int index, ObjectHandler* value) {
    local_variables[index] = value;
}

ObjectHandler* Frame::getStackItem(u4 index) {
    return local_stack.get(index);
}

unsigned int Frame::getLocalVariablesSize() {
    return local_variables.size();
}

unsigned int Frame::getStackSize() {
    return local_stack.size();
}

JavaClass* Frame::getMethodOwnerClass() {
    return method->owner_class;
}

#ifdef DEBUG
string Frame::getMethodName() {
    char* method_name;
    // local_variables[0] matches 'this'.
    if (!method->owner_class->getStringFromConstantPool(method->name_index, method_name))
        throw runtime_error("Frame::getMethodName() failed!");
    return string(method_name);
}
#endif