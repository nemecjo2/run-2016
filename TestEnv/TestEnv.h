#ifndef TESTENV_H
#define TESTENV_H

#include "../DataTypes.h"

/**
 * Call test function and prints corresponding messages.
 * @param function Function to be called.
 * @param test_name Test name.
 * @param suite_name Suit name.
 */
void call(void (&)(const char*), const char*, const char*);

/**
 * Prints fail message.
 * @param test_name Test name.
 * @param message Message to display.
 */
void testFailed(const char* test_name, const char* message);

/**
 * Tests if pointer is null.
 * @param result Tested pointer.
 * @param test_name Test name.
 * @param message Message to display in case of fail.
 * @return bool True if result is NULL, false otherwise.
 */
bool assertNull(void * result, const char* test_name, const char* message);

/**
 * Tests if pointer is not null.
 * @param result Tested pointer.
 * @param test_name Test name.
 * @param message Message to display in case of fail.
 * @return bool True if result is not NULL, false otherwise.
 */
bool assertNotNull(void * result, const char* test_name, const char* message);

template <typename T>
bool _assertEquals(bool, T, T, char *, char *);

/**
 * Tests if u1 parameters are equal.
 * @param expected Expected u1.
 * @param result Tested u1.
 * @param test_name Test name.
 * @param message Message to display in case of fail.
 * @return bool True if expected == result, false otherwise.
 */
bool assertEquals(u1 expected, u1 result, const char* test_name, const char* message);

/**
 * Tests if u2 parameters are equal.
 * @param expected Expected u2.
 * @param result Tested u2.
 * @param test_name Test name.
 * @param message Message to display in case of fail.
 * @return bool True if expected == result, false otherwise.
 */
bool assertEquals(u2 expected, u2 result, const char* test_name, const char* message);

/**
 * Tests if s2 parameters are equal.
 * @param expected Expected s2.
 * @param result Tested s2.
 * @param test_name Test name.
 * @param message Message to display in case of fail.
 * @return bool True if expected == result, false otherwise.
 */
bool assertEquals(s2 expected, s2 result, const char* test_name, const char* message);

/**
 * Tests if u4 parameters are equal.
 * @param expected Expected u4.
 * @param result Tested u4.
 * @param test_name Test name.
 * @param message Message to display in case of fail.
 * @return bool True if expected == result, false otherwise.
 */
bool assertEquals(u4 expected, u4 result, const char* test_name, const char* message);

/**
 * Tests if s4 parameters are equal.
 * @param expected Expected s4.
 * @param result Tested s4.
 * @param test_name Test name.
 * @param message Message to display in case of fail.
 * @return bool True if expected == result, false otherwise.
 */
bool assertEquals(s4 expected, s4 result, const char* test_name, const char* message);

/**
 * Tests if string parameters are equal.
 * @param expected Expected string.
 * @param result Tested string.
 * @param test_name Test name.
 * @param message Message to display in case of fail.
 * @return bool True if expected == result, false otherwise.
 */
bool assertEquals(const char* expected, const char* result, const char* test_name, const char* message);

#endif