#include <cstdlib>
#include <cstring>
#include <string>
#include <iostream>
#include "TestEnv.h"

using namespace std;

void call(void (&function)(const char*), const char* test_name, const char* suite_name) {
    cout << "%TEST_STARTED% " << test_name << " (" << suite_name << ")" << endl;
    cout << suite_name << " : " << test_name << endl;
    string label(test_name);
    label.append(" (").append(suite_name).append(")");
    function(label.c_str());
    cout << "%TEST_FINISHED% time=0 " << test_name << " (" << suite_name << ")" << endl;
    cout << "==============================================" << endl << endl;
}

void testFailed(const char* test_name, const char* message) {
    cout << "%TEST_FAILED% time=0 testname=" << test_name;
    cout << " message=" << message << endl;
}

bool assertNull(void * result, const char* test_name, const char* message) {
    if (result == NULL)
        return true;
    testFailed(test_name, message);
    cout << "Expected: NULL" << endl;
    cout << "-------------------------" << endl;
    return false;
}

bool assertNotNull(void * result, const char* test_name, const char* message) {
    if (result != NULL)
        return true;
    testFailed(test_name, message);
    cout << "Expected: NOT NULL" << endl;
    cout << "-------------------------" << endl;
    return false;
}
template <typename T>
bool _assertEquals(bool comparison, T expected, T result, const char* test_name, const char* message) {
    if (comparison == false) {
        testFailed(test_name, message);
        cout << "Expected: " << expected << ", Result: " << result << endl;
        cout << "-------------------------" << endl;
    }

    return comparison;
}

bool assertEquals(u1 expected, u1 result, const char* test_name, const char* message) {
    return _assertEquals(expected == result, expected, result, test_name, message);
}

bool assertEquals(u2 expected, u2 result, const char* test_name, const char* message) {
    cout << "???????????????" << message << "| " << expected << " ? " << result << endl;
    return _assertEquals(expected == result, expected, result, test_name, message);
}

bool assertEquals(u4 expected, u4 result, const char* test_name, const char* message) {
    return _assertEquals(expected == result, expected, result, test_name, message);
}

bool assertEquals(s4 expected, s4 result, const char* test_name, const char* message) {
    return _assertEquals(expected == result, expected, result, test_name, message);
}

bool assertEquals(const char* expected, const char* result, const char* test_name, const char* message) {
    return _assertEquals(strcmp(expected, result) == 0, expected, result, test_name, message);
}