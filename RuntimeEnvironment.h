#ifndef RUNTIMEENVIRONMENT_H
#define RUNTIMEENVIRONMENT_H

#include <map>

#include "FileManager.h"
#include "ClassHeap.h"
#include "ObjectHeap.h"
#include "FrameStack.h"

using namespace std;

class RuntimeEnvironment {
    public:
        /**
         * Runtime Environment constructor.
         * @param heap_size Requested heap size. Maximal heap size is 2^32 - 1. TODO Set minimal heap size.
         * @param classes_path Path to folder with classes.
         * @param class_name Class to be executed.
         * @param main_method_name Method to be executed.
         * @param argc
         * @param argv
         */
         RuntimeEnvironment(uint32_t, const char*, const char*, const char*, int, char**);
        ~RuntimeEnvironment();

        /**
         * Method to Run the programm.
         * @return int Result of run.
         */
        ObjectHandler* run();

    private:
        void returnVoidCase();
        void returnValueCase();

        void gotoCase();

        void newCase();

        void newArrayCase();
        void anewArrayCase();
        void arrayLengthCase();

        void putFieldCase(u1);
        void getFieldCase(u1);

        void popCase();
        void dupCase();
        void dupx1Case();
        void dup2Case();

        void i2cCase();

        void aconst_nullCase();

        void iconstCase(u1);

        void iastoreCase();
        void istoreCase();
        void aastoreCase();
        void astore_nCase(u1);
        void istore_nCase(u1);

        void ialoadCase();
        void iloadCase();
        void aaloadCase();
        void aload_nCase(u1);
        void iload_nCase(u1);

        void iaddCase();
        void isubCase();
        void imulCase();

        void iincCase();

        void iorCase();
        void iandCase();

        void bipushCase();
        void sipushCase();

        void invokevirtualCase();
        void invokespecialCase();

        void ifCase(u1);
        void if_icmpCase(u1);

        void ldcCase();

        void clinit(JavaClass*);
        void callNative(char*, ObjectHandler**, JavaClass*, ObjectHandler*);

        FileManager open_files;

        FrameStack frame_stack;
        ClassHeap class_heap;
        ObjectHeap object_heap;

        ObjectHandler* return_value;
        Frame* actual_frame;

        #ifdef STACK_DUMP
        void stack_dump();
        #endif
};

#endif