## Description ##
* JVM implemented in C++

## Implemented functionality ##
* Control structures (if/else, for, while)
* Dynamic memory allocation (new)
* Arrays (int[], Object[])
* Inheritance
* Input (from stdin/file) and output (stdout)
* Mark and sweep Garbage collecting

## Repository structure description ##
### Root folders ###
* **examples/** Examples in Java, both compiled classes and source code.
* **lib/** Custom implementation of needed java.lang / java.io classes.
* **nbproject/** NetBeans project files.
* **TestEnv/** Test Environment classes needed for tests to execute.
* **tests/** Test files to execute.

### Root files ###
* **ClassHeap.h/cpp** JVM Heap of class definitions.
* **Constants.h** Constants definition class.
* **DataTypes.h/cpp** Definition of all data types used in our JVM.
* **FileManager.h/cpp** Class to manage reading from file.
* **Frame.h/cpp** JVM Frame class.
* **FrameStack.h/cpp** JVM Frame Stack class.
* **GarbageCollector.h/cpp** JVM Garbage Collector class.
* **InstructionSet.h** Java instructions definition class.
* **JavaClass.h/cpp** Class to manage Java class parsing.
* **JavaClassPrinter.h/cpp** Java class printer used for debug.
* **Makefile**
* **README.md**
* **RuntimeEnvironment.h/cpp** JVM Runtime class.
* **Stack.h/cpp** Generic Stack class.
* **main.cpp** Main file to run the project.

## How to run your Java file ##
* run "make all" command
* Compile your Java file normally by javac
* run "./dist/Debug/GNU-Linux/run <root folder> <folder to main class> <main method>"

## Examples ##
### Working with stdin and stdout ###
* ./dist/Debug/GNU-Linux/run examples console/Console input
* ./dist/Debug/GNU-Linux/run examples console/Console output

### Reading from file ###
* ./dist/Debug/GNU-Linux/run examples files/OurFile main

### Garbage making and collecting ###
* ./dist/Debug/GNU-Linux/run examples garbage_collector/Array main

### SAT Solver ###
SAT Solver takes input from file and returns output on stdout.

#### SAT input file syntax ####
```
#!
<literals count> <clauses count>
<clause 1 size = n>
<literal 1> <literal 2> ... <literal n>
<clause 2 size = m>
<literal 1> <literal 2> ... <literal m>
...
```

* <literals count> is **integer**, range 1 - 9
* <clauses count> is **integer**, range 1 - infinity
* <clause i size> is **integer**, range 1 - infinity
* <literal i> is **integer**, range 1 - <literals count>,
where negated literal is specified by "-" before <literal_i>

#### SAT output syntax ####

```
#!
Result: <result>
[<literal 1 setup>, <literal 2 setup>, ... , <literal n setup> ]
```
* <result> is **TRUE** if Formula is satisfiable, **FALSE** otherwise
* <literal i setup> is 1 if <literal i> should be set to true, 0 if <literal_i> should be set to false. Setup of all literals should satisfy the Formula, if Formula is satisfiable.

#### How to run SAT Solver ####
* Specify in examples/satsolver/SATSolver.java file with input.
* Compile examples/satsolver/SATSolver.java normally by javac.
* Run "./dist/Debug/GNU-Linux/run examples satsolver/SATSolver main"

### Test files ###
* Test files are located in tests/ folder and can be runned from Netbeans.

## Contributors ##

* Josef Němeček (nemecjo2@fit.cvut.cz)
* David Šenkýř (senkyda1@fit.cvut.cz)