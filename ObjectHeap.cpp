#include <stdexcept>
#include <cstring>

#include "ObjectHeap.h"
#include "Constants.h"
#include "JavaClass.h"

ObjectHeap::ObjectHeap(u4 heap_size, FrameStack* frame_stack, ClassHeap* class_heap, FileManager* open_files) : heap_size(heap_size) {
    memory = new Variable[heap_size];
    memory_usages = new bool[heap_size];
    memset(memory_usages, false, heap_size);

    garbage_collector = new GarbageCollector(heap_size, memory, memory_usages, frame_stack, class_heap, open_files);

    #ifdef MEMORY_DUMP
    this->frame_stack = frame_stack;
    #endif    
}

ObjectHeap::~ObjectHeap() {}

Variable* ObjectHeap::getFreeSpace(u2 size) {
    #ifdef MEMORY_DUMP
    memoryDump();
    #endif

    u4 index = _getFreeSpace(size);
    if (index == heap_size) {
        // Garbage Collector.
        garbage_collector->collectGarbage();

        index = _getFreeSpace(size);
        if (index == heap_size)
            throw runtime_error("Cannot allocate memory");
    }

    for (u2 i = 0; i < size; ++i)
        memory_usages[index + i] = true;

    return memory + index;
}

u4 ObjectHeap::_getFreeSpace(u2 size) {
    bool found = false;
    u4 i = 0;
    for (; i < heap_size;) {
        if (memory_usages[i] == false) {
            found = true;
            for (u2 j = 1; j < size; ++j) {
                if (i + j >= heap_size)
                    return heap_size;

                if (memory_usages[i+j] == true) {
                    found = false;
                    break;
                }
            }
            if (found == true)
                return i;
            i += size;
        }
        else
            ++i;
    }

    return heap_size;
}

///////////////////////////////////////////////////////
// Create object handler methods.
ObjectHandler* ObjectHeap::createIntHandler(s4 value) {
    ObjectHandler* handler = new ObjectHandler();

    handler->type = FIELD_INT;
    handler->heap_ptr = getFreeSpace(1);
    handler->heap_ptr->_int = value;
    handler->class_definition = NULL;

    return handler;
}

ObjectHandler* ObjectHeap::createBoolHandler(u1 value) {
    ObjectHandler* handler = new ObjectHandler();

    handler->type = FIELD_BOOL;
    handler->heap_ptr = getFreeSpace(1);
    handler->heap_ptr->_char = value;
    handler->class_definition = NULL;

    return handler;
}

ObjectHandler* ObjectHeap::createStringHandler(JavaClass* class_definition, u2 string_index) {
    ObjectHandler* handler = new ObjectHandler();

    handler->type = FIELD_STRING;
    handler->heap_ptr = getFreeSpace(1);
    handler->heap_ptr->_short = string_index;
    handler->class_definition = class_definition;

    return handler;
}

ObjectHandler* ObjectHeap::createObjectHandler(JavaClass* javaclass) {
    ObjectHandler* handler = new ObjectHandler();

    // Initialize handle
    handler->type = FIELD_CLASS;
    handler->heap_ptr = getFreeSpace(1+javaclass->fields_count);
    handler->heap_ptr->_int = javaclass->fields_count;
    memset(handler->heap_ptr+1, 0, javaclass->fields_count);
    handler->class_definition = javaclass;

    return handler;
}

ObjectHandler* ObjectHeap::createStaticHandler(JavaClass* javaclass) {
    ObjectHandler* handler = new ObjectHandler();

    handler->type = FIELD_CLASS;
    handler->heap_ptr = NULL;
    handler->class_definition = javaclass;

    return handler;
}

ObjectHandler* ObjectHeap::createArrayHandler(u4 size, u1 type) {
    ObjectHandler* handler = new ObjectHandler();

    // Initialize handle
    handler->type = FIELD_ARRAY;
    handler->heap_ptr = getFreeSpace(2+size);
    handler->heap_ptr->_int = size;
    (handler->heap_ptr+1)->_char = type;
    memset(handler->heap_ptr+2, 0, size);
    handler->class_definition = NULL;

    return handler;
}

ObjectHandler* ObjectHeap::createArrayHandler(u4 size, JavaClass* javaclass) {
    ObjectHandler* handler = new ObjectHandler();

    // Initialize handle
    handler->type = FIELD_ARRAY;
    handler->heap_ptr = getFreeSpace(2+size);
    handler->heap_ptr->_int = size;
    (handler->heap_ptr+1)->_char = FIELD_CLASS;
    memset(handler->heap_ptr+2, 0, size);
    handler->class_definition = javaclass;

    return handler;
}

ObjectHandler* ObjectHeap::createEmptyHandler() {
    ObjectHandler* handler = new ObjectHandler();
    handler->type = 0;
    handler->heap_ptr = getFreeSpace(1);
    handler->heap_ptr->_int = 0;
    handler->class_definition = NULL;
    return handler;
}

#ifdef MEMORY_DUMP
void ObjectHeap::memoryDump() {
    printf("\t\t\t Memory Dump\n\t\t\t");
    for (u4 i = 0; i < heap_size;) {
        printf("%d ", memory_usages[i]);
        
        if (++i % 20 == 0)
            printf("\n\t\t\t");
    }

    printf("\n\t\t\t -----------\n");
    
    u4 index;
    ObjectHandler* object_handler;
    Frame* actual;
    bool* memory_marks = new bool[heap_size];
    memset(memory_marks, false, heap_size);
    
    for (u4 i = 0; i < frame_stack->size(); ++i) {
        actual = frame_stack->getFrameAt(i);
        
        for (unsigned int j = 0; j < actual->getLocalVariablesSize(); ++j) {
            object_handler = actual->getLocalVariable(j);
            
            if (object_handler == NULL)
                continue;
            index = object_handler->heap_ptr - memory;
            memory_marks[index] = true;
        }
        for (unsigned int j = 0; j < actual->getStackSize(); ++j) {
            object_handler = actual->getStackItem(j);

            index = object_handler->heap_ptr - memory;
            memory_marks[index] = true;
        }
    }
    
    printf("\t\t\t Live objects\n\t\t\t");
    for (u4 i = 0; i < heap_size;) {
        printf("%d ", memory_marks[i]);
        
        if (++i % 20 == 0)
            printf("\n\t\t\t");
    }

    printf("\n\t\t\t -----------\n");
}
#endif