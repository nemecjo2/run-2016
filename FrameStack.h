#ifndef FRAMESTACK_H
#define FRAMESTACK_H

#include "DataTypes.h"
#include "JavaClass.h"
#include "Stack.h"
#include "Frame.h"

#include <iostream>

using namespace std;

class FrameStack {
    public:

        /**
         * Create Frame on FrameStack.
         * @param MethodItem* Pointer to Method.
         * @param ObjectHandler* Class instance.
         */
        void createFrame(MethodItem*, ObjectHandler*);

        /**
         * Get actual running frame.
         * @return Pointer to actual running Frame.
         */
        Frame* getActualFrame();

        /**
         * Get frame at given index.
         * @param u4 index.
         * @return Pointer to Frame at given index.
         */
        Frame* getFrameAt(u4);

        /**
         * Pop actual running frame from FrameStack.
         * @return False if stack is empty, true otherwise.
         */
        bool popActualFrame();

        /**
         * Is FrameStack empty?
         * @return True if stack is empty, false otherwise.
         */
        bool isEmpty();

        /**
         * Size of stack.
         * @return u4 size of stack.
         */
        u4 size();

    private:
        Stack<Frame*> frame_stack;
};

#endif