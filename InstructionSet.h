#ifndef INSTRUCTION_SET_H
#define INSTRUCTION_SET_H

#define nop 0               //0
#define aconst_null 1       //1             push a null reference onto the stack

#define iconst_m1 2         //02
#define iconst_0 3          //03
#define iconst_1 4          //04
#define iconst_2 5          //05
#define iconst_3 6          //06
#define iconst_4 7          //07
#define iconst_5 8          //08

#define bipush 16           //10    +1B     push a byte to stack as int value
#define sipush  17          //11    +2B     push a short to stack

#define ldc 18              //12    +1B     push a constant with index onto the stack
#define iload 21            //15    +1B     load an int value from a local variable #index

#define aload 25            //19    +1B     load a reference onto the stack from a local variable #index

#define iload_0 26          //1A            load an int from local variable
#define iload_1 27          //1B
#define iload_2 28          //1C
#define iload_3 29          //1D

#define lload_0 30          //1E            load a long from local variable 
#define lload_1 31          //1F
#define lload_2 32          //20
#define lload_3 33          //21

#define aload_0 42          //2A
#define aload_1 43          //2B
#define aload_2 44          //2C
#define aload_3 45          //2D
#define iaload 46           //2E            load an int from an array

#define aaload 50           //32            load onto the stack a reference from an array

#define istore 54           //36    +1B     store int value into variable #index

#define astore 58           //3A    +1B     store a reference into a local variable #index

#define istore_0 59         //3B
#define istore_1 60         //3C
#define istore_2 61         //3D
#define istore_3 62         //3E

#define astore_0 75         //4B
#define astore_1 76         //4C
#define astore_2 77         //4D
#define astore_3 78         //4E

#define iastore 79          //4F            store int into array (arrayRef, index, value)

#define aastore 83          //53            store reference into array (arrayRef, index, objectref)

#define pop 87              //57            pop the top operand stack value

#define dup 89              //59            duplicate the value on top of the stack
#define dup_x1 90           //5A            insert a copy of the top value into the stack two values from the top. value1 and value2 must not be of the type double or long.
#define dup2 92             //5C            duplicate the top one or two operand stack values 

#define iadd 96             //60            add two ints
#define isub 100            //64            sub two ints
#define imul 104            //68            multiply two integers

#define iand 126            //7E            bitwise AND int
#define ior 128             //80            bitwise OR int

#define iinc 132            //84    +2B     increment local variable #index by signed byte const

#define i2c 146             //92            convert int to char

#define ifeq 153            //99    +2B     if value is 0, branch to instruction at branchoffset
#define ifne 154            //9A    +2B     if value is not equal to 0, branch to instruction at branchoffset
#define iflt 155            //9B    +2B     if value is less than 0, branch to instruction at branchoffset
#define ifge 156            //9C    +2B     if value is greater than or equal to 0, branch to instruction at branchoffset
#define ifgt 157            //9D    +2B     if value is greater than 0, branch to instruction at branchoffset
#define ifle 158            //9E    +2B     if value is less than or equal to 0, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)

#define if_icmpeq 159       //9F    +2B     succeeds if and only if value1 = value2
#define if_icmpne 160       //A0    +2B     succeeds if and only if value1 ≠ value2
#define if_icmplt 161       //A1    +2B     succeeds if and only if value1 < value2
#define if_icmpge 162       //A2    +2B     succeeds if and only if value1 >= value2
#define if_icmpgt 163       //A3    +2B     succeeds if and only if value1 > value2
#define if_icmple 164       //A4    +2B     succeeds if and only if value1 <= value2

#define _goto 167           //A7    +2B     goes to another instruction at branchoffset

#define ireturn 172         //AC            return an integer from a method
#define areturn 176         //B0            return a reference from a method
#define _return 177         //B1            return from void method

#define getstatic 178       //B2    +2B     get static field value of class
#define putstatic 179       //B3    +2B     set static field to value in a class, where the field is identified by a field reference index in constant pool

#define getfield 180        //B4    +2B     get a field value of an object objectref, where the field is identified by field reference in the constant pool index
#define putfield 181        //B5    +2B     set field to value in an object objectref, where the field is identified by a field reference index in constant pool

#define invokevirtual 182   //B6    +2B     invoke virtual method ono object objectref and put result on stack

#define invokespecial 183   //B7    +2B
#define invokestatic 184    //B8    +2B

#define _new 187            //BB    +2B     create new object of type identified by class reference in constant pool index
#define newarray 188        //BC    +1B     create new array - count is on stack, +1B atype tag
#define anewarray 189       //BD    +2B     create a new array of references of length count and component type identified by the class reference index in the constant pool
#define arraylength 190     //BE            get length of array

#define _if_null 198        //C6    +2B     if value is null, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)
#define _if_nonnull 199     //C7    +2B     if value is null, branch to instruction at branchoffset (signed short constructed from unsigned bytes branchbyte1 << 8 + branchbyte2)

#endif