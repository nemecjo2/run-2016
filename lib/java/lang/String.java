package java.lang;

public class String {
    
    public String toString()
    {
        return this;
    }
    
    public native String[] split(String separator);
    
    public static String valueOf(Object obj) {
        return (obj == null) ? "null" : obj.toString();
    }


}
