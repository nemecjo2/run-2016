package java.io;

public class PrintStream extends java.lang.Object
{
	public native void print(java.lang.String string);
	public native void print(char character);
	public native void print(int integer);

	public native void println(java.lang.String string);
	public native void println(char character);
	public native void println(int integer);

	public String toString()
	{
		return "";
	}
}