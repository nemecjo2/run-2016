package java.io;

public class FileReader {
    public File file;

    public FileReader(File file) {
        this.file = file;
    }

    public FileReader(String fileName) {
        this.file = new File(fileName);
    }

  public native int read();
}