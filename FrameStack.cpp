#include "FrameStack.h"

void FrameStack::createFrame(MethodItem* method, ObjectHandler* class_instance) {
    #ifdef DEBUG
    printf("New frame created\n");
    #endif
    frame_stack.push(new Frame(method, class_instance));
}

Frame* FrameStack::getActualFrame() {
    return frame_stack.top();
}

Frame* FrameStack::getFrameAt(u4 index) {
    return frame_stack.get(index);
}

bool FrameStack::popActualFrame() {
    if (frame_stack.empty())
        return false;

    Frame* top = frame_stack.top();
    frame_stack.pop();
    delete top;
    return true;
}

bool FrameStack::isEmpty() {
    return frame_stack.empty();
}

u4 FrameStack::size() {
    return frame_stack.size();
}