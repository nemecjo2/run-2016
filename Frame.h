#ifndef FRAME_H
#define FRAME_H

#include <vector>
#include <stdexcept>

#include "Stack.h"
#include "DataTypes.h"
#include "Constants.h"
#include "JavaClass.h"

class Frame {
    public:
        /**
         * Creates new Frame for given Method and Class instance.
         * @param method MethodItem from CP.
         * @param class_instance Class instance from Object heap.
         */
         Frame(MethodItem*, ObjectHandler*);
        ~Frame();

        /**
         * Gets next byte of method code.
         * @return u1 Next byte of method code.
         */
        u1 getNext();

        /**
         * Pushes value on the operand stack.
         * @param operand Value to be pushed on the operand stack.
         */
        void pushOnStack(ObjectHandler*);

        /**
         * Pops value from the operand stack.
         * @return Value popped from the operand stack.
         */
        ObjectHandler* popFromStack();

        /**
         * Gets local variable for given index.
         * @param index Index of local variable.
         * @return ObjectHandler* Local variable value.
         */
        ObjectHandler* getLocalVariable(int);

        /**
         * Sets local variable to given value.
         * @param index Index of local variable.
         * @param value Value to be set.
         */
        void setLocalVariable(int, ObjectHandler*);

        /**
         * Gets item from local stack for given index.
         * @param index Index of item in local stack.
         * @return ObjectHandler* Local stack item.
         */
        ObjectHandler* getStackItem(u4);

        /**
         * Gets local stack size.
         * @return unsigned int Stack size.
         */
        unsigned int getLocalVariablesSize();

        /**
         * Gets local stack size.
         * @return unsigned int Stack size.
         */
        unsigned int getStackSize();

        /**
         * Gets class definition for method's actual class instance (this) = object.
         * @return JavaClass* pointer to class definition.
         */
        JavaClass* getMethodOwnerClass();

        #ifdef DEBUG
        string getMethodName();
        #endif
        
        /**
         * Programm counter.
         */
        int pc;

    private:
        MethodItem* method;

        vector<ObjectHandler*> local_variables;
        Stack<ObjectHandler*> local_stack;
};

#endif