#ifndef GARBAGECOLLECTOR_H
#define GARBAGECOLLECTOR_H

#include <map>

#include "DataTypes.h"
#include "FrameStack.h"
#include "Frame.h"
#include "ClassHeap.h"
#include "FileManager.h"

using namespace std;

class GarbageCollector {
    public:
         GarbageCollector(u4 heap_size, Variable*, bool*, FrameStack*, ClassHeap*, FileManager*);
        ~GarbageCollector();

        void collectGarbage();

    private:
        void mark();
        void sweep();

        void markObjectHandler(ObjectHandler*);

        u4 heap_size;
        Variable* memory;
        bool* memory_usages;
        FrameStack* frame_stack;
        ClassHeap* class_heap;
        FileManager* open_files;
        bool* memory_marks;
};

#endif