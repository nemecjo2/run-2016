#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <cstdio>
#include <map>

#include "DataTypes.h"

using namespace std;

class FileManager {
    public:
        FileManager();
        ~FileManager();

        FILE* getFile(ObjectHandler*, char*);
        void closeFile(ObjectHandler*);

    private:
        map<ObjectHandler*, FILE*> open_files;
};

#endif