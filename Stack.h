#ifndef STACK_H
#define STACK_H

#include <cstdlib>
#include <vector>
#include <stdexcept>


#include "DataTypes.h"

using namespace std;

template <typename T>
class Stack {
    public: 
        void push(T);
        void pop();
        T top();
        T get(u4);
        bool empty();
        u4 size();

    private: 
        vector<T> elements;
};

template <class T>
void Stack<T>::push(T item) {
    elements.push_back(item);
}

template <class T>
void Stack<T>::pop() { 
    if (elements.empty())
       throw out_of_range("Stack<>::pop(): empty stack!");

    elements.pop_back();
}

template <class T>
T Stack<T>::top() { 
   if (elements.empty()) 
      throw out_of_range("Stack<>::top(): empty stack!");

   return elements.back();
}

template <class T>
T Stack<T>::get(u4 index) {
    if (elements.empty())
      throw out_of_range("Stack<>::top(): empty stack!");

    return elements.at(index);
}

template <class T>
bool Stack<T>::empty() {
    return elements.empty();
}

template <class T>
u4 Stack<T>::size() {
    return elements.size();
}

#endif