#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include "JavaClass.h"
#include "RuntimeEnvironment.h"

using namespace std;

int main(int argc, char** argv) {    
    try {
        if (argc < 4 || argc > 5) {
            cout << "Invalid arguments count. Usage: 'run <root folder> <main class> <main method>' or ";
            cout << "'run <root folder> <main class> <main method> <heap size> '" << endl;
        }

        int heap_size = DEFAULT_HEAP_SIZE;
        char* root_folder = argv[1];
        char* main_class = argv[2];
        char* main_method = argv[3];

        if (argc == 5)
            heap_size = atoi(argv[4]);

        RuntimeEnvironment runtime(heap_size, root_folder, main_class, main_method, 0, NULL);
        ObjectHandler* result = runtime.run();
        if (result == NULL) {
            cout << "Returned NULL" << endl;
        } else if (result->type == FIELD_INT) {
            cout << "Returned INT " << result->heap_ptr->_int << endl;
        }
        return EXIT_SUCCESS;
    } catch (runtime_error e) {
        cout << e.what() << endl;
        return EXIT_FAILURE;
    }
}