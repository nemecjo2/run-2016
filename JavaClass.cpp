#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include "JavaClass.h"
#include "Constants.h"
#include "JavaClassPrinter.h"
#include "ClassHeap.h"
#include "ObjectHeap.h"

JavaClass::JavaClass(ClassHeap* class_heap) : public_protected_fields_count(0), initialized(false) {
    this->class_heap = class_heap;
    this->object_heap = class_heap->object_heap;
}

bool JavaClass::fetch(FILE* classfile) {
    char * reader;
    int bufferSize;

    fseek(classfile, 0, SEEK_END);
    bufferSize = ftell(classfile)+1;

    char * buffer = new char[bufferSize];
    reader = buffer;
    fseek(classfile, 0, SEEK_SET);
    fread((void *) buffer, 1, bufferSize, classfile);

    // Check magic
    magic = readu4(reader);
    if (magic != 0xCAFEBABE) {
        return false;
    }

    minor_version = readu2(reader);
    major_version = readu2(reader);
    constant_pool_count = readu2(reader);
    constant_pool = new ConstantPoolItem* [constant_pool_count];

    for (u2 i=1; i<constant_pool_count; ++i) {
        constant_pool[i] = loadConstantPoolItem(reader);

        // If constant type is double or long, constant pool takes two entries, second entry is set to NULL.
        if (constant_pool[i]->tag == CONSTANT_Double || constant_pool[i]->tag == CONSTANT_Long) {
            constant_pool[++i] = NULL;
        }
    }

    access_flags = readu2(reader);
    this_class = readu2(reader);
    super_class = readu2(reader);

    interfaces_count = readu2(reader);
    interfaces = new u2[interfaces_count];
    for (int i = 0; i < interfaces_count; ++i) {
        interfaces[i] = readu2(reader);
    }

    u2 inherited_fields_count = getInheritedFieldsCount();
    fields_count = readu2(reader) + inherited_fields_count;
    fields = new FieldItem* [fields_count];
    getInheritedFields();
    for (int i = inherited_fields_count; i < fields_count; ++i) {
        fields[i] = loadFieldItem(reader);
    }

    methods_count = readu2(reader);
    methods = new MethodItem* [methods_count];
    for (int i = 0; i < methods_count; ++i) {
        methods[i] = loadMethodItem(reader);
    }

    attributes_count = readu2(reader);
    attributes = new AttributeItem* [attributes_count];
    for (int i=0; i < attributes_count; ++i) {
        attributes[i] = loadAttributeItem(reader);
    }

    #ifdef DEBUG
        JavaClassPrinter* printer = new JavaClassPrinter();
        printer->javaclass = this;
        printer->printJavaClass();
    #endif

    return true;
}

ConstantPoolItem * JavaClass::loadConstantPoolItem(char* &reader) {
    u1 tag = readu1(reader);

    switch(tag) {
        case CONSTANT_Class:
            return loadClassConstantPoolItem(reader);
        case CONSTANT_Fieldref:
            return loadFieldrefConstantPoolItem(reader);
        case CONSTANT_Methodref:
            return loadMethodrefConstantPoolItem(reader);
        case CONSTANT_InterfaceMethodref:
            return loadInterfaceMethodrefConstantPoolItem(reader);
        case CONSTANT_String:
            return loadStringConstantPoolItem(reader);
        case CONSTANT_Integer:
            return loadIntegerConstantPoolItem(reader);
        case CONSTANT_Float:
            return loadFloatConstantPoolItem(reader);
        case CONSTANT_Long:
            return loadLongConstantPoolItem(reader);
        case CONSTANT_Double:
            return loadDoubleConstantPoolItem(reader);
        case CONSTANT_NameAndType:
            return loadNameAndTypeConstantPoolItem(reader);
        case CONSTANT_Utf8:
            return loadUtf8ConstantPoolItem(reader);
        case CONSTANT_MethodHandle:
            return loadMethodHandleConstantPoolItem(reader);
        case CONSTANT_MethodType:
            return loadMethodTypeConstantPoolItem(reader);
        case CONSTANT_InvokeDynamic:
            return loadInvokeDynamicConstantPoolItem(reader);
        default: // Unreachable statement.
            return loadGeneralConstantPoolItem(reader, tag);
    }
}

GeneralConstantPoolItem * JavaClass::loadGeneralConstantPoolItem(char* &reader, u1 tag) {
    GeneralConstantPoolItem * item = new GeneralConstantPoolItem();
    item->tag = tag;
    item->content = (u1*)reader;
    reader += getConstantPoolItemSize(item);

    return item;
}

ClassConstantPoolItem * JavaClass::loadClassConstantPoolItem(char* &reader) {
    ClassConstantPoolItem * item = new ClassConstantPoolItem();
    item->tag = CONSTANT_Class;
    item->name_index = readu2(reader);

    return item;
}

FieldrefConstantPoolItem * JavaClass::loadFieldrefConstantPoolItem(char* &reader) {
    FieldrefConstantPoolItem * item = new FieldrefConstantPoolItem();
    item->tag = CONSTANT_Fieldref;
    item->class_index = readu2(reader);
    item->name_and_type_index = readu2(reader);

    return item;
}

MethodrefConstantPoolItem * JavaClass::loadMethodrefConstantPoolItem(char* &reader) {
    MethodrefConstantPoolItem * item = new MethodrefConstantPoolItem();
    item->tag = CONSTANT_Methodref;
    item->class_index = readu2(reader);
    item->name_and_type_index = readu2(reader);

    return item;
}

InterfaceMethodrefConstantPoolItem * JavaClass::loadInterfaceMethodrefConstantPoolItem(char* &reader) {
    InterfaceMethodrefConstantPoolItem * item = new InterfaceMethodrefConstantPoolItem();
    item->tag = CONSTANT_InterfaceMethodref;
    item->class_index = readu2(reader);
    item->name_and_type_index = readu2(reader);

    return item;
}

StringConstantPoolItem * JavaClass::loadStringConstantPoolItem(char* &reader) {
    StringConstantPoolItem * item = new StringConstantPoolItem();
    item->tag = CONSTANT_String;
    item->string_index = readu2(reader);

    return item;
}

IntegerConstantPoolItem * JavaClass::loadIntegerConstantPoolItem(char* &reader) {
    IntegerConstantPoolItem * item = new IntegerConstantPoolItem();
    item->tag = CONSTANT_Integer;
    item->bytes = readu4(reader);

    return item;
}

FloatConstantPoolItem * JavaClass::loadFloatConstantPoolItem(char* &reader) {
    FloatConstantPoolItem * item = new FloatConstantPoolItem();
    item->tag = CONSTANT_Float;
    item->bytes = readu4(reader);

    return item;
}

LongConstantPoolItem * JavaClass::loadLongConstantPoolItem(char* &reader) {
    LongConstantPoolItem * item = new LongConstantPoolItem();
    item->tag = CONSTANT_Long;
    item->high_bytes = readu4(reader);
    item->low_bytes = readu4(reader);

    return item;
}

DoubleConstantPoolItem * JavaClass::loadDoubleConstantPoolItem(char* &reader) {
    DoubleConstantPoolItem * item = new DoubleConstantPoolItem();
    item->tag = CONSTANT_Double;
    item->high_bytes = readu4(reader);
    item->low_bytes = readu4(reader);

    return item;
}

NameAndTypeConstantPoolItem * JavaClass::loadNameAndTypeConstantPoolItem(char* &reader) {
    NameAndTypeConstantPoolItem * item = new NameAndTypeConstantPoolItem();
    item->tag = CONSTANT_NameAndType;
    item->name_index = readu2(reader);
    item->descriptor_index = readu2(reader);

    return item;
}

Utf8ConstantPoolItem * JavaClass::loadUtf8ConstantPoolItem(char* &reader) {
    Utf8ConstantPoolItem * item = new Utf8ConstantPoolItem();
    item->tag = CONSTANT_Utf8;
    item->length = readu2(reader);
    item->bytes = (u1*)reader;
    reader += item->length;

    return item;
}

MethodHandleConstantPoolItem * JavaClass::loadMethodHandleConstantPoolItem(char* &reader) {
    MethodHandleConstantPoolItem * item = new MethodHandleConstantPoolItem();
    item->tag = CONSTANT_MethodHandle;
    item->reference_kind = readu1(reader);
    item->reference_index = readu1(reader);

    return item;
}

MethodTypeConstantPoolItem * JavaClass::loadMethodTypeConstantPoolItem(char* &reader) {
    MethodTypeConstantPoolItem * item = new MethodTypeConstantPoolItem();
    item->tag = CONSTANT_MethodType;
    item->descriptor_index = readu2(reader);

    return item;
}

InvokeDynamicConstantPoolItem * JavaClass::loadInvokeDynamicConstantPoolItem(char* &reader) {
    InvokeDynamicConstantPoolItem * item = new InvokeDynamicConstantPoolItem();
    item->tag = CONSTANT_InvokeDynamic;
    item->bootstrap_method_attr_index = readu2(reader);
    item->name_and_type_index = readu2(reader);

    return item;
}

MethodItem * JavaClass::loadMethodItem(char* &reader) {
    MethodItem * item = new MethodItem();
    item->flags = readu2(reader);
    item->name_index = readu2(reader);
    item->descriptor_index = readu2(reader);
    item->attributes_count = readu2(reader);
    item->attributes = new AttributeItem* [item->attributes_count];
    item->method_code = NULL;
    item->owner_class = this;

    bool code = false;
    for (int i = 0; i < item->attributes_count; ++i) {
        // If it is a Code attribute, save it also to code.
        u2 name_index = looku2(reader);
        char* string;
        getStringFromConstantPool(name_index, string);
        if (strcmp(string, NM_CODE) == 0)
            code = true;

        item->attributes[i] = loadAttributeItem(reader);
        if (code) {
            item->method_code = (CodeAttributeItem*)item->attributes[i];
            code = false;
        }
    }

    item->method_parameters_cnt = getMethodParametersCount(item->descriptor_index);
    return item;
}

FieldItem * JavaClass::loadFieldItem(char* &reader) {
    FieldItem * item = new FieldItem();
    item->flags = readu2(reader);
    item->name_index = readu2(reader);
    item->descriptor_index = readu2(reader);
    item->attributes_count = readu2(reader);
    item->attributes = new AttributeItem* [item->attributes_count];
    item->constant_value = NULL;
    item->value = NULL;
    item->owner_class = this;

    bool constant = false;
    for (u2 i = 0; i < item->attributes_count; ++i) {
        // If it is a Constant Value attribute, save it also to Constant.
        u2 name_index = looku2(reader);
        char* string;
        getStringFromConstantPool(name_index, string);
        if (strcmp(string, NM_CONSTANT_VALUE) == 0)
            constant = true;

        item->attributes[i] = loadAttributeItem(reader);
        if (constant) {
            item->constant_value = (ConstantValueAttributeItem*)item->attributes[i];
            constant = false;
        }

        if (item->flags & MAF_STATIC) {
            item->value = object_heap->createEmptyHandler();
            if ((item->constant_value != NULL)) {
                ConstantPoolItem* cp_item = constant_pool[item->constant_value->constantvalue_index];
                // TODO SWITCH LIKE IN Load Constant Pool Item?
                if (cp_item->tag == CONSTANT_Integer) {
                    IntegerConstantPoolItem* integer = (IntegerConstantPoolItem*) cp_item;
                    item->value->type = FIELD_INT;
                    item->value->heap_ptr->_int = integer->bytes;
                }
                else if (cp_item->tag == CONSTANT_String) {
                    StringConstantPoolItem* string_item = (StringConstantPoolItem*) cp_item;
                    item->value->type = FIELD_STRING;
                    item->value->heap_ptr->_short = string_item->string_index;
                }
            }
        }
    }

    if (item->flags & MAF_PUBLIC || item->flags & MAF_PROTECTED)
        ++public_protected_fields_count;

    return item;
}

AttributeItem * JavaClass::loadAttributeItem(char* &reader) {
    u2 name_index = readu2(reader);
    u4 length = readu4(reader);
    char* string;

    // Decision based on the constant pool
    getStringFromConstantPool(name_index, string);

    if (strcmp(string, NM_CODE) == 0)
        return loadCodeAttributeItem(reader, name_index, length);
    else if (strcmp(string, NM_CONSTANT_VALUE) == 0)
        return loadConstantValueAttributeItem(reader, name_index, length);
    else
        return loadGeneralAttributeItem(reader, name_index, length);
}

AttributeItem * JavaClass::loadGeneralAttributeItem(char* &reader, u2 name_index, u4 length) {
    GeneralAttributeItem * item = new GeneralAttributeItem();
    item->type = ATTR_BASIC;
    item->name_index = name_index;
    item->length = length;

    item->info = new u1[item->length];
    for (u4 i = 0; i < item->length; ++i) {
        item->info[i] = readu1(reader);
    }

    return item;
}

ConstantValueAttributeItem * JavaClass::loadConstantValueAttributeItem(char* &reader, u2 name_index, u4 length) {
    ConstantValueAttributeItem * item = new ConstantValueAttributeItem();
    item->type = ATTR_CONSTANT_VALUE;
    item->constantvalue_index = readu2(reader);

    return item;
}

CodeAttributeItem * JavaClass::loadCodeAttributeItem(char* &reader, u2 name_index, u4 length) {
    CodeAttributeItem * item = new CodeAttributeItem();
    item->type = ATTR_CODE;
    item->name_index = name_index;
    item->length = length;

    item->max_stack = readu2(reader);
    item->max_locals = readu2(reader);

    item->code_length = readu4(reader);
    item->code = new u1[item->code_length];
    for (u4 i = 0; i < item->code_length; ++i) {
        item->code[i] = readu1(reader);
    }

    item->exception_table_length = readu2(reader);
    item->exception_table = new ExceptionItem* [item->exception_table_length];
    for (u2 i = 0; i < item->exception_table_length; ++i) {
        item->exception_table[i] = loadExceptionItem(reader);
    }

    item->attributes_count = readu2(reader);
    item->attributes = new AttributeItem* [item->attributes_count];
    for (u2 i = 0; i < item->attributes_count; ++i) {
        item->attributes[i] = loadAttributeItem(reader);
    }

    return item;
}

ExceptionItem * JavaClass::loadExceptionItem(char* &reader) {
    ExceptionItem * item = new ExceptionItem();
    item->start_pc = readu2(reader);
    item->end_pc = readu2(reader);
    item->handler_pc = readu2(reader);
    item->catch_type = readu2(reader);

    return item;
}

int JavaClass::getConstantPoolItemSize(ConstantPoolItem* item) {
    switch(item->tag) {
        case CONSTANT_Utf8:
            return 2+looku2(((GeneralConstantPoolItem*)item)->content);
        case CONSTANT_Integer:
            return 4;
        case CONSTANT_Float:
            return 4;
        case CONSTANT_Long:
            return 8;
        case CONSTANT_Double:
            return 8;
        case CONSTANT_String:
            return 2;
        case CONSTANT_Fieldref:
            return 4;
        case CONSTANT_Methodref:
            return 4;
        case CONSTANT_InterfaceMethodref:
            return 4;
        case CONSTANT_NameAndType:
            return 4;
        case CONSTANT_MethodHandle:
            return 3;
        case CONSTANT_MethodType:
            return 2;
        case CONSTANT_InvokeDynamic:
            return 4;
        default:
            return 0;
    }
}

bool JavaClass::getStringFromConstantPool(u2 i, char*& output) {
    if (i < 1 || i >= constant_pool_count)
        return false;

    ConstantPoolItem* cp_item = constant_pool[i];

    if (cp_item->tag != CONSTANT_Utf8)
        return false;

    Utf8ConstantPoolItem* utf8_cp_item = (Utf8ConstantPoolItem*) cp_item;
    output = new char[utf8_cp_item->length+1];
    memcpy(output, utf8_cp_item->bytes, utf8_cp_item->length);
    output[utf8_cp_item->length] = 0;

    return true;
}

void JavaClass::getName(char*& output) {
    u2 index = ((ClassConstantPoolItem*) constant_pool[this_class])->name_index;
    getStringFromConstantPool(index, output);
}

string JavaClass::getName() {
    char* class_name;
    getName(class_name);
    return string(class_name);
}

string JavaClass::getParentName() {
    u2 index = ((ClassConstantPoolItem*) constant_pool[super_class])->name_index;
    char* class_name;
    getStringFromConstantPool(index, class_name);
    return string(class_name);
}

bool JavaClass::hasParent() {
    return super_class != 0;
}

bool JavaClass::hasParent(JavaClass* javaclass) {
    if (hasParent() == false)
        return false;

    JavaClass* parent = class_heap->getClass(getParentName());
    return parent == javaclass ? true : parent->hasParent(javaclass);
}

JavaClass* JavaClass::getParent() {
    return hasParent() ? class_heap->getClass(getParentName()) : NULL;
}

bool JavaClass::hasMethod(const char* name) {
    char* method_name;
    for (int i=0; i<methods_count; ++i) {
        getStringFromConstantPool(methods[i]->name_index, method_name);

        if (strcmp(name, method_name) == 0)
            return true;
    }
    return false;
}

MethodItem* JavaClass::getMethod(const char* name) {
    char* method_name;
    for (int i=0; i<methods_count; ++i) {
        getStringFromConstantPool(methods[i]->name_index, method_name);

        if (strcmp(name, method_name) == 0)
            return methods[i];
    }
    if (hasParent()) {
        return class_heap->getClass(getParentName())->getMethod(name);
    }
    string message("Method ");
    message.append(name).append(" does not exits in ").append(getName());
    throw runtime_error(message);
}

MethodItem* JavaClass::getMethod(const char* name, const char* descriptor) {
    char* method_name;
    char* method_descriptor;
    for (int i=0; i<methods_count; ++i) {
        getStringFromConstantPool(methods[i]->name_index, method_name);

        if (strcmp(name, method_name) == 0) {
            getStringFromConstantPool(methods[i]->descriptor_index, method_descriptor);
 
            if (strcmp(descriptor, method_descriptor) == 0)
                return methods[i];            
        }
    }
    if (hasParent()) {
        return class_heap->getClass(getParentName())->getMethod(name, descriptor);
    }
    string message("Method ");
    message.append(name).append(" does not exits in ").append(getName());
    throw runtime_error(message);
}

MethodItem* JavaClass::getLocalMethod(const char* name, const char* descriptor) {
    char* method_name;
    char* method_descriptor;
    for (int i=0; i<methods_count; ++i) {
        getStringFromConstantPool(methods[i]->name_index, method_name);

        if (strcmp(name, method_name) == 0) {
            delete [] method_name;
            getStringFromConstantPool(methods[i]->descriptor_index, method_descriptor);

            if (strcmp(descriptor, method_descriptor) == 0) {
                delete [] method_descriptor;
                return methods[i];
            }

            delete [] method_descriptor;
        }
    }

    return NULL;
}

MethodItem* JavaClass::getMethod(const char* method_name, const char* method_descriptor, JavaClass* referenced_class, bool invoke_special) {
    JavaClass* super_class = this;
    bool super_class_found = false;
    if (referenced_class != NULL && invoke_special == false) {
        while (super_class != NULL) {
            if (super_class == referenced_class) {
                super_class_found = true;
                break;
            }

            super_class = super_class->hasParent() ? super_class->getParent() : NULL;
        }
    }

    if (referenced_class == NULL || referenced_class == this || super_class_found) {
        MethodItem* method = getLocalMethod(method_name, method_descriptor);
        if (method != NULL)
            return method;
        else if (hasParent() == true)
            return getParent()->getMethod(method_name, method_descriptor, getParent(), invoke_special);
    }

    if (hasParent() == true)
        return getParent()->getMethod(method_name, method_descriptor, referenced_class, invoke_special);

    string message("Method lookup for method ");
    message.append(method_name).append(" failed in class ").append(getName());
    throw runtime_error(message);
}

JavaClass* JavaClass::getClassForMethod(const char* name) {
    char* method_name;
    for (int i=0; i<methods_count; ++i) {
        getStringFromConstantPool(methods[i]->name_index, method_name);

        if (strcmp(name, method_name) == 0)
            return this;
    }
    if (hasParent()) {
        return class_heap->getClass(getParentName())->getClassForMethod(name);
    }
    string message("Method ");
    message.append(name).append(" does not exits in ").append(getName());
    throw runtime_error(message);
}


u2 JavaClass::getFieldIndex(const char* name) {
    for (u2 i = 0; i < fields_count; ++i) {
        char* field_name;
        fields[i]->owner_class->getStringFromConstantPool(fields[i]->name_index, field_name);
        if (strcmp(name, field_name) == 0)
            return i;
    }

    string message("Field ");
    message.append(name).append(" does not exits in ").append(getName());
    throw runtime_error(message);
}

int JavaClass::getMethodParametersCount(u2 index) {
    char* descriptor;
    getStringFromConstantPool(index, descriptor);
    int count = 0;
    int length = strlen(descriptor);

    for (int i=1; i<length; ++i) {
        if (descriptor[i] == 'L') {
            while (descriptor[i] != ';') {
                ++i;
            }
            ++count;
            continue;
        }
        if (descriptor[i] == ')') {
            break;
        }
        ++count;
    }
    return count;
}

u2 JavaClass::getInheritedFieldsCount() {
    u2 count = 0;
    JavaClass* parent = getParent();
    while (parent != NULL) {
        count += parent->public_protected_fields_count;
        parent = parent->getParent();
    }

    return count;
}

void JavaClass::getInheritedFields() {
    if (hasParent() == false)
        return;

    u2 index = 0;
    FieldItem* field;
    JavaClass* parent = getParent();

    for (u2 i = 0; i < parent->fields_count; ++i) {
        field = parent->fields[i];

        if (field->flags & MAF_PUBLIC || field->flags & MAF_PROTECTED) {
            if (field->flags & MAF_STATIC) {
                fields[index++] = field;
                continue;
            }

            FieldItem * item = new FieldItem();
            item->flags = field->flags;
            item->name_index = field->name_index;
            item->descriptor_index = field->descriptor_index;
            item->attributes_count = field->attributes_count;
            item->attributes = field->attributes;
            item->constant_value = NULL;
            item->value = NULL;
            item->owner_class = field->owner_class;
            fields[index++] = item;
        }
    }
}