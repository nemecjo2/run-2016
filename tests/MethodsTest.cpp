#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include "TestEnv/TestEnv.h"
#include "Constants.h"
#include "DataTypes.h"
#include "RuntimeEnvironment.h"

using namespace std;

/*
 * Simple C++ Test Suite.
 */
#define TEST_FILE_NAME "Methods"

void MethodCall(const char*);
void MethodArgumentsCall(const char*);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% " << TEST_FILE_NAME << endl;
    cout << "%SUITE_STARTED%" << endl;

    call(MethodCall, "MethodCall", TEST_FILE_NAME);
    call(MethodArgumentsCall, "MethodArgumentsCall", TEST_FILE_NAME);

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

void MethodCall(const char* test_name) {
    cout << "Method call verification" << endl;
    try {
        RuntimeEnvironment runtime(10, "examples", "test_files/MethodCall", "main", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(42, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void MethodArgumentsCall(const char* test_name) {
    cout << "Method call verification" << endl;
    try {
        RuntimeEnvironment runtime(10, "examples", "test_files/MethodCall", "methodArguments", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(42, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}