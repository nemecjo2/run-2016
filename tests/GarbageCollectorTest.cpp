#include <stdlib.h>
#include <iostream>

#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include "TestEnv/TestEnv.h"
#include "Constants.h"
#include "DataTypes.h"
#include "RuntimeEnvironment.h"

using namespace std;

/*
 * Simple C++ Test Suite.
 */
#define TEST_FILE_NAME "Array"

void GarbageCollector(const char*);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% Array" << endl;
    cout << "%SUITE_STARTED%" << endl;

    call(GarbageCollector, "GarbageCollector", TEST_FILE_NAME);

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

void GarbageCollector(const char* test_name) {
    try {
        RuntimeEnvironment runtime(100, "examples", "garbage_collector/Array", "main", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(0, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}