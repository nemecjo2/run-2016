#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string.h>
#include "TestEnv/TestEnv.h"
#include "Constants.h"
#include "DataTypes.h"
#include "RuntimeEnvironment.h"

using namespace std;

/*
 * Simple C++ Test Suite.
 */
#define TEST_FILE_NAME "Runtime"

void SimpleArithmeticClassHeapSize(const char*);
void SimpleArithmeticClassHeapSize2(const char*);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% Runtime" << endl;
    cout << "%SUITE_STARTED%" << endl;

    call(SimpleArithmeticClassHeapSize, "SimpleArithmeticClassHeapSize", TEST_FILE_NAME);
    call(SimpleArithmeticClassHeapSize2, "SimpleArithmeticClassHeapSize2", TEST_FILE_NAME);

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

void SimpleArithmeticClassHeapSize(const char* test_name) {
    cout << "Minimal heap size verification" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/SimpleArithmeticClass", "add", 0, NULL);
        runtime.run();
    } catch (runtime_error e) {
        string message("[Too small heap size for run] ");
        message.append(e.what());
        testFailed(test_name, message.c_str());
    }
}

void SimpleArithmeticClassHeapSize2(const char* test_name) {
    cout << "Insufficient heap size verification" << endl;
    try {
        RuntimeEnvironment runtime(3, "examples", "test_files/SimpleArithmeticClass", "add", 0, NULL);
        runtime.run();
        testFailed(test_name, "Insufficient heap size for run");
    } catch (runtime_error e) {
        if (!strcmp("Cannot allocate memory", e.what()) == 0) {
            testFailed(test_name, e.what());
        }
    }
}