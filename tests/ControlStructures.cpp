#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include "TestEnv/TestEnv.h"
#include "Constants.h"
#include "DataTypes.h"
#include "RuntimeEnvironment.h"

using namespace std;

/*
 * Simple C++ Test Suite.
 */
#define TEST_FILE_NAME "ControlStructures"

void PositiveIntCondition(const char*);
void NegativelyPositiveIntCondition(const char*);

void NegativeIntCondition(const char*);
void NegativelyNegativeIntCondition(const char*);

void LessIntCondition(const char*);
void NegativeLessIntCondition(const char*);

void LessOrEqualIntCondition(const char*);
void NegativeLessOrEqualIntCondition(const char*);

void GreaterIntCondition(const char*);
void NegativeGreaterIntCondition(const char*);

void GreaterOrEqualIntCondition(const char*);
void NegativeGreaterOrEqualIntCondition(const char*);

void IntCondition(const char*);

void PositiveBoolCondition(const char*);
void NegativelyPositiveBoolCondition(const char*);

void NegativeBoolCondition(const char*);
void NegativelyNegativeBoolCondition(const char*);

void PositiveZeroCondition(const char*);
void NegativelyPositiveZeroCondition(const char*);

void NegativeZeroCondition(const char*);
void NegativelyNegativeZeroCondition(const char*);

void LessZeroCondition(const char*);
void NegativeLessZeroCondition(const char*);

void LessOrEqualZeroCondition(const char*);
void NegativeLessOrEqualZeroCondition(const char*);

void GreaterZeroCondition(const char*);
void NegativeGreaterZeroCondition(const char*);

void GreaterOrEqualZeroCondition(const char*);
void NegativeGreaterOrEqualZeroCondition(const char*);

void SimpleForIntLoop(const char*);
void SimpleWhileIntLoop(const char*);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% Runtime" << endl;
    cout << "%SUITE_STARTED%" << endl;

    call(PositiveIntCondition, "PositiveIntCondition", TEST_FILE_NAME);
    call(NegativelyPositiveIntCondition, "NegativelyPositiveIntCondition", TEST_FILE_NAME);

    call(NegativeIntCondition, "NegativeIntCondition", TEST_FILE_NAME);
    call(NegativelyNegativeIntCondition, "NegativelyNegativeIntCondition", TEST_FILE_NAME);

    call(LessIntCondition, "LessIntCondition", TEST_FILE_NAME);
    call(NegativeLessIntCondition, "NegativeLessIntCondition", TEST_FILE_NAME);

    call(LessOrEqualIntCondition, "LessOrEqualIntCondition", TEST_FILE_NAME);
    call(NegativeLessOrEqualIntCondition, "NegativeLessOrEqualIntCondition", TEST_FILE_NAME);

    call(GreaterIntCondition, "GreaterIntCondition", TEST_FILE_NAME);
    call(NegativeGreaterIntCondition, "NegativeGreaterIntCondition", TEST_FILE_NAME);

    call(GreaterOrEqualIntCondition, "GreaterOrEqualIntCondition", TEST_FILE_NAME);
    call(NegativeGreaterOrEqualIntCondition, "NegativeGreaterOrEqualIntCondition", TEST_FILE_NAME);

    call(IntCondition, "IntCondition", TEST_FILE_NAME);

    call(PositiveBoolCondition, "PositiveBoolCondition", TEST_FILE_NAME);
    call(NegativelyPositiveBoolCondition, "NegativelyPositiveBoolCondition", TEST_FILE_NAME);

    call(NegativeBoolCondition, "NegativeBoolCondition", TEST_FILE_NAME);
    call(NegativelyNegativeBoolCondition, "NegativelyNegativeBoolCondition", TEST_FILE_NAME);

    call(PositiveZeroCondition, "PositiveZeroCondition", TEST_FILE_NAME);
    call(NegativelyPositiveZeroCondition, "NegativelyPositiveZeroCondition", TEST_FILE_NAME);

    call(NegativeZeroCondition, "NegativeZeroCondition", TEST_FILE_NAME);
    call(NegativelyNegativeZeroCondition, "NegativelyNegativeZeroCondition", TEST_FILE_NAME);

    call(LessZeroCondition, "LessZeroCondition", TEST_FILE_NAME);
    call(NegativeLessZeroCondition, "NegativeLessZeroCondition", TEST_FILE_NAME);

    call(LessOrEqualZeroCondition, "LessOrEqualZeroCondition", TEST_FILE_NAME);
    call(NegativeLessOrEqualZeroCondition, "NegativeLessOrEqualZeroCondition", TEST_FILE_NAME);

    call(GreaterZeroCondition, "GreaterZeroCondition", TEST_FILE_NAME);
    call(NegativeGreaterZeroCondition, "NegativeGreaterZeroCondition", TEST_FILE_NAME);

    call(GreaterOrEqualZeroCondition, "GreaterOrEqualZeroCondition", TEST_FILE_NAME);
    call(NegativeGreaterOrEqualZeroCondition, "NegativeGreaterOrEqualZeroCondition", TEST_FILE_NAME);

    call(SimpleForIntLoop, "SimpleForIntLoop", TEST_FILE_NAME);
    call(SimpleWhileIntLoop, "SimpleWhileIntLoop", TEST_FILE_NAME);

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

void PositiveIntCondition(const char* test_name) {
    cout << "Positive integer condition" << endl;
    try {
        RuntimeEnvironment runtime(10, "examples", "test_files/Conditions", "positiveIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(21, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativelyPositiveIntCondition(const char* test_name) {
    cout << "Negatively positive integer condition" << endl;
    try {
        RuntimeEnvironment runtime(10, "examples", "test_files/Conditions", "negativelyPositiveIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(3, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeIntCondition(const char* test_name) {
    cout << "Negative integer condition" << endl;
    try {
        RuntimeEnvironment runtime(5, "examples", "test_files/Conditions", "negativeIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(4, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativelyNegativeIntCondition(const char* test_name) {
    cout << "Negatively negative integer condition" << endl;
    try {
        RuntimeEnvironment runtime(5, "examples", "test_files/Conditions", "negativelyNegativeIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(4, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void LessIntCondition(const char* test_name) {
    cout << "Less integer condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "lessIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(152, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeLessIntCondition(const char* test_name) {
    cout << "Negative less integer condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "negativeLessIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(1881, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void LessOrEqualIntCondition(const char* test_name) {
    cout << "Less or equal integer condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "lessOrEqualIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(155, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeLessOrEqualIntCondition(const char* test_name) {
    cout << "Negative less or equal integer condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "negativeLessOrEqualIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(16, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void GreaterIntCondition(const char* test_name) {
    cout << "Greater integer condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "greaterIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(11, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeGreaterIntCondition(const char* test_name) {
    cout << "Negative greater integer condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "negativeGreaterIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(11, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void GreaterOrEqualIntCondition(const char* test_name) {
    cout << "Greater or equal integer condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "greaterOrEqualIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(200, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeGreaterOrEqualIntCondition(const char* test_name) {
    cout << "Negative greater or equal integer condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "negativeGreaterOrEqualIntCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(14, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void IntCondition(const char* test_name) {
    cout << "Simple integer condition with else branch" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "intCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(12, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void PositiveBoolCondition(const char* test_name) {
    cout << "Positive boolean condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "positiveBoolCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(20, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativelyPositiveBoolCondition(const char* test_name) {
    cout << "Negatively positive boolean condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "negativelyPositiveBoolCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(35, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeBoolCondition(const char* test_name) {
    cout << "Negative boolean condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "negativeBoolCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(13, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativelyNegativeBoolCondition(const char* test_name) {
    cout << "Negatively negative boolean condition" << endl;
    try {
        RuntimeEnvironment runtime(4, "examples", "test_files/Conditions", "negativelyNegativeBoolCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(72, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void PositiveZeroCondition(const char* test_name) {
    cout << "Positive zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "positiveZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(-1, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativelyPositiveZeroCondition(const char* test_name) {
    cout << "Negatively positive zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "negativelyPositiveZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(12, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeZeroCondition(const char* test_name) {
    cout << "Negative zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "negativeZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(36, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativelyNegativeZeroCondition(const char* test_name) {
    cout << "Negatively negative zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "negativelyNegativeZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(1, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void LessZeroCondition(const char* test_name) {
    cout << "Less zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "lessZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(154, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeLessZeroCondition(const char* test_name) {
    cout << "Negative less zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "negativeLessZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(1882, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void LessOrEqualZeroCondition(const char* test_name) {
    cout << "Less or equal zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "lessOrEqualZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(2012, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeLessOrEqualZeroCondition(const char* test_name) {
    cout << "Negative less or equal zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "negativeLessOrEqualZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(18, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void GreaterZeroCondition(const char* test_name) {
    cout << "Greater zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "greaterZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(35, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeGreaterZeroCondition(const char* test_name) {
    cout << "Negative greater zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "negativeGreaterZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(8, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void GreaterOrEqualZeroCondition(const char* test_name) {
    cout << "Greater or equal zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "greaterOrEqualZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(202, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void NegativeGreaterOrEqualZeroCondition(const char* test_name) {
    cout << "Negative greater or equal zero condition" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Conditions", "negativeGreaterOrEqualZeroCondition", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(-14, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

///////////////////////////////////////////////////////
// Loops
void SimpleForIntLoop(const char* test_name) {
    cout << "Simple integer for loop" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Loops", "simpleForIntLoop", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(10, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void SimpleWhileIntLoop(const char* test_name) {
    cout << "Simple integer while loop" << endl;
    try {
        RuntimeEnvironment runtime(40, "examples", "test_files/Loops", "simpleWhileIntLoop", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(3, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}