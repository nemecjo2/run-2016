#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include "TestEnv/TestEnv.h"
#include "Constants.h"
#include "DataTypes.h"
#include "RuntimeEnvironment.h"

using namespace std;

/*
 * Simple C++ Test Suite.
 */
#define TEST_FILE_NAME "Inheritance"

void InheritedMethod(const char*);
void InheritedFields(const char*);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% Inheritance" << endl;
    cout << "%SUITE_STARTED%" << endl;

    call(InheritedMethod, "InheritedMethod", TEST_FILE_NAME);
    call(InheritedFields, "InheritedFields", TEST_FILE_NAME);

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

void InheritedMethod(const char* test_name) {
    try {
        RuntimeEnvironment runtime(100, "examples", "inheritance/MainClass", "main", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(11, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void InheritedFields(const char* test_name) {
    try {
        RuntimeEnvironment runtime(100, "examples", "inheritance/MainClass", "inheritedFields", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(92, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}