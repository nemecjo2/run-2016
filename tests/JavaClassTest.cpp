#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string.h>
#include "TestEnv/TestEnv.h"
#include "JavaClass.h"
#include "Constants.h"
#include "ClassHeap.h"
#include "ObjectHeap.h"

using namespace std;

/*
 * Simple C++ Test Suite
 */
#define TEST_FILE_NAME "Javaclass"

void SimpleClass(const char*);
void SimpleClassConstantPoolItems(const char*);
void SimpleClassFieldItems(const char*);
void SimpleClassMethodItems(const char*);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% JavaClass" << endl;
    cout << "%SUITE_STARTED%" << endl;

    call(SimpleClass, "SimpleClass", TEST_FILE_NAME);
    call(SimpleClassConstantPoolItems, "SimpleClassConstantPoolItems", TEST_FILE_NAME);
    call(SimpleClassFieldItems, "SimpleClassFieldItems", TEST_FILE_NAME);
    call(SimpleClassMethodItems, "SimpleClassMethodItems", TEST_FILE_NAME);

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

void SimpleClass(const char* test_name) {
    FILE * classFile = fopen("examples/test_files/SimpleClass.class", "r");
    if (!assertNotNull(classFile, test_name, "Error while opening file"))
        return;

    ClassHeap* classHeap = new ClassHeap("examples");
    JavaClass* c = new JavaClass(classHeap);
    c->object_heap = new ObjectHeap(100, new FrameStack(), classHeap, NULL);
    c->fetch(classFile);

    assertEquals((u2)0, c->minor_version, test_name, "Minor Version");
    assertEquals((u2)51, c->major_version, test_name, "Major Version");
    assertEquals((u2)51, c->constant_pool_count, test_name, "Constant Pool Count");
    assertEquals((u2)(CAF_SUPER | CAF_PUBLIC), c->access_flags, test_name, "Class Access Flags");
    assertEquals((u2)10, c->this_class, test_name, "This Class");
    assertEquals((u2)11, c->super_class, test_name, "Super Class");
    assertEquals((u2)0, c->interfaces_count, test_name, "Interfaces Count");
    assertEquals((u2)3, c->fields_count, test_name, "Fields Count");
    assertEquals((u2)3, c->methods_count, test_name, "Methods Count");
    assertEquals((u2)1, c->attributes_count, test_name, "Attributes Count");

    cout << "Class Attributes verification" << endl;
    assertEquals((u2)ATTR_BASIC, c->attributes[0]->type, test_name, "Attribute type");
}

void SimpleClassConstantPoolItems(const char* test_name) {
    FILE * classFile = fopen("examples/test_files/SimpleClass.class", "r");
    if (!assertNotNull(classFile, test_name, "Error while opening file"))
        return;

    ClassHeap* classHeap = new ClassHeap("test_files");
    JavaClass* c = new JavaClass(classHeap);
    c->object_heap = new ObjectHeap(100, new FrameStack(), classHeap, NULL);
    c->fetch(classFile);

    u2 index, name_index, type_index;
    char * this_class_name, * super_class_name;
    char * integer_name, * integer_type;
    char * string_name, * string_type;
    char * iterator_name, * iterator_return_type;
    char * constructor_name, * constructor_return_type;

    /* This class name test. */
    c->getName(this_class_name);
    assertEquals("cz/cvut/fit/ddw/pagerank/SimpleClass", this_class_name, test_name, "This Class name");

    /* Super class name test. */
    index = ((ClassConstantPoolItem*) c->constant_pool[c->super_class])->name_index;
    c->getStringFromConstantPool(index, super_class_name);
    assertEquals("java/lang/Object", super_class_name, test_name, "Super Class name");


    /* Integer field test. */
    int integer_field_cp_index = 1;
    index = ((FieldrefConstantPoolItem*) c->constant_pool[integer_field_cp_index])->name_and_type_index;
    name_index = ((NameAndTypeConstantPoolItem*) c->constant_pool[index])->name_index;
    type_index = ((NameAndTypeConstantPoolItem*) c->constant_pool[index])->descriptor_index;

    c->getStringFromConstantPool(name_index, integer_name);
    assertEquals("integerField", integer_name, test_name, "Integer field name");
    c->getStringFromConstantPool(type_index, integer_type);
    assertEquals("I", integer_type, test_name, "Integer field type");

    /* String field test. */
    int string_field_cp_index = 9;
    index = ((FieldrefConstantPoolItem*) c->constant_pool[string_field_cp_index])->name_and_type_index;
    name_index = ((NameAndTypeConstantPoolItem*) c->constant_pool[index])->name_index;
    type_index = ((NameAndTypeConstantPoolItem*) c->constant_pool[index])->descriptor_index;

    c->getStringFromConstantPool(name_index, string_name);
    assertEquals("stringField", string_name, test_name, "String field name");
    c->getStringFromConstantPool(type_index, string_type);
    assertEquals("Ljava/lang/String;", string_type, test_name, "String field type");

    /* Iterator method test. */
    int iterator_method_cp_index = 7;
    index = ((MethodrefConstantPoolItem*) c->constant_pool[iterator_method_cp_index])->name_and_type_index;
    name_index = ((NameAndTypeConstantPoolItem*) c->constant_pool[index])->name_index;
    type_index = ((NameAndTypeConstantPoolItem*) c->constant_pool[index])->descriptor_index;

    c->getStringFromConstantPool(name_index, iterator_name);
    assertEquals("iterator", iterator_name, test_name, "Iterator method name");
    c->getStringFromConstantPool(type_index, iterator_return_type);
    assertEquals("()Ljava/util/Iterator;", iterator_return_type, test_name, "Iterator method name");

    /* Constructor method test. */
    int constructor_method_cp_index = 8;
    index = ((MethodrefConstantPoolItem*) c->constant_pool[constructor_method_cp_index])->name_and_type_index;
    name_index = ((NameAndTypeConstantPoolItem*) c->constant_pool[index])->name_index;
    type_index = ((NameAndTypeConstantPoolItem*) c->constant_pool[index])->descriptor_index;

    c->getStringFromConstantPool(name_index, constructor_name);
    assertEquals("<init>", constructor_name, test_name, "Constructor method name");
    c->getStringFromConstantPool(type_index, constructor_return_type);
    assertEquals("()V", constructor_return_type, test_name, "Constructor method name");

    cout << "Class Attributes verification" << endl;
    assertEquals((u2)ATTR_BASIC, c->attributes[0]->type, test_name, "Attribute type");
}

void SimpleClassFieldItems(const char* test_name) {
    FILE * classFile = fopen("examples/test_files/SimpleClass.class", "r");
    if (!assertNotNull(classFile, test_name, "Error while opening file"))
        return;

    ClassHeap* classHeap = new ClassHeap("test_files");
    JavaClass* c = new JavaClass(classHeap);
    c->object_heap = new ObjectHeap(100, new FrameStack(), classHeap, NULL);
    c->fetch(classFile);

    assertEquals((u2)0, c->getFieldIndex("integerField"), test_name, "Integer field index");
    assertEquals((u2)1, c->getFieldIndex("stringField"), test_name, "String field index");
    assertEquals((u2)2, c->getFieldIndex("charField"), test_name, "Char field index");

    FieldItem * fields[3];
    FieldItem * f0 = new FieldItem();
    f0->attributes_count = 0;
    f0->descriptor_index = 13;
    f0->flags = (MAF_PRIVATE);
    f0->name_index = 12;

    FieldItem * f1 = new FieldItem();
    f1->attributes_count = 0;
    f1->descriptor_index = 15;
    f1->flags = (MAF_PUBLIC);
    f1->name_index = 14;

    FieldItem * f2 = new FieldItem();
    f2->attributes_count = 1;
    f2->descriptor_index = 17;
    f2->flags = (MAF_PRIVATE | MAF_STATIC | MAF_FINAL);
    f2->name_index = 16;

    fields[0] = f0;
    fields[1] = f1;
    fields[2] = f2;

    for (int i=0; i<c->fields_count; i++) {
        FieldItem * res = c->fields[i];
        FieldItem * exp = fields[i];
        cout << "Field item no. " << i << " verification" << endl;
        if (!assertNotNull(res, test_name, "Field Item"))
            return;

        assertEquals(exp->attributes_count, res->attributes_count, test_name, "Attributes Count");
        assertEquals(exp->flags, res->flags, test_name, "Flags");
        assertEquals(exp->descriptor_index, res->descriptor_index, test_name, "Descriptor index");
        assertEquals(exp->name_index, res->name_index, test_name, "Name index");
    }

    cout << "Field item no. 2 Attributes verification" << endl;
    assertEquals((u2)ATTR_CONSTANT_VALUE, c->fields[2]->attributes[0]->type, test_name, "Attribute Type");
}

void SimpleClassMethodItems(const char* test_name) {
    FILE * classFile = fopen("examples/test_files/SimpleClass.class", "r");
    if (!assertNotNull(classFile, test_name, "Error while opening file"))
        return;

    ClassHeap* classHeap = new ClassHeap("test_files");
    JavaClass* c = new JavaClass(classHeap);
    c->object_heap = new ObjectHeap(100, new FrameStack(), classHeap, NULL);
    c->fetch(classFile);

    /* JavaClass::getMethod test. */
    assertNotNull(c->getMethod("voidMethod"), test_name, "Method voidMethod");
    assertNotNull(c->getMethod("iterator"), test_name, "Method iterator");

    try {
        c->getMethod("not_existing_method");
        testFailed(test_name, "Method not_existing_method");
    } catch (runtime_error e) {
        if (!strcmp("Method not_existing_method does not exits in java/lang/Object", e.what()) == 0) {
            testFailed(test_name, e.what());
        }
    }

    MethodItem * methods[3];
    MethodItem * f0 = new MethodItem();
    f0->attributes_count = 1;
    f0->descriptor_index = 21;
    f0->flags = (MAF_PROTECTED);
    f0->name_index = 20;

    MethodItem * f1 = new MethodItem();
    f1->attributes_count = 2;
    f1->descriptor_index = 28;
    f1->flags = (MAF_PUBLIC);
    f1->name_index = 27;

    MethodItem * f2 = new MethodItem();
    f2->attributes_count = 1;
    f2->descriptor_index = 36;
    f2->flags = (MAF_PUBLIC);
    f2->name_index = 35;

    methods[0] = f0;
    methods[1] = f1;
    methods[2] = f2;

    for (int i=0; i<c->methods_count; i++) {
        MethodItem * res = c->methods[i];
        MethodItem * exp = methods[i];
        cout << "Method item no. " << i << " verification" << endl;
        if (!assertNotNull(res, test_name, "Method Item"))
            return;

        assertEquals(exp->attributes_count, res->attributes_count, test_name, "Attributes Count");
        assertEquals(exp->flags, res->flags, test_name, "Flags");
        assertEquals(exp->descriptor_index, res->descriptor_index, test_name, "Descriptor index");
        assertEquals(exp->name_index, res->name_index, test_name, "Name index");
    }

    cout << "Method item no. 0 Attributes verification" << endl;
    assertEquals((u2)ATTR_CODE, c->methods[0]->attributes[0]->type, test_name, "Attribute Type");

    cout << "Method item no. 1 Attributes verification" << endl;
    assertEquals((u2)ATTR_CODE, c->methods[1]->attributes[0]->type, test_name, "Attribute Type");
    assertEquals((u2)ATTR_BASIC, c->methods[1]->attributes[1]->type, test_name, "Attribute Type");

    cout << "Method item no. 2 Attributes verification" << endl;
    assertEquals((u2)ATTR_CODE, c->methods[2]->attributes[0]->type, test_name, "Attribute Type");
}