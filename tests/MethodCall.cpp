#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include "TestEnv/TestEnv.h"
#include "Constants.h"
#include "DataTypes.h"
#include "RuntimeEnvironment.h"

using namespace std;

/*
 * Simple C++ Test Suite.
 */
#define TEST_FILE_NAME "Runtime"

void SimpleArithmeticClassHeapSize(const char*);
void SimpleArithmeticClassHeapSize2(const char*);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% Runtime" << endl;
    cout << "%SUITE_STARTED%" << endl;

    cout << "%TEST_STARTED% SimpleArithmeticClassHeapSize (Runtime)" << endl;
    SimpleArithmeticClassHeapSize("SimpleArithmeticClassHeapSize (Runtime)");
    cout << "%TEST_FINISHED% time=0 SimpleArithmeticClassHeapSize (Runtime)" << endl;
    cout << "==============================================" << endl << endl;

    cout << "%TEST_STARTED% SimpleArithmeticClassHeapSize2 (Runtime)" << endl;
    SimpleArithmeticClassHeapSize2("SimpleArithmeticClass (Runtime)");
    cout << "%TEST_FINISHED% time=0 SimpleArithmeticClassHeapSize2 (Runtime)" << endl;
    cout << "==============================================" << endl << endl;

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

void SimpleArithmeticClassHeapSize(const char* test_name) {
    cout << TEST_FILE_NAME << " : " << test_name << endl;
    cout << "Minimal heap size verification" << endl;
    try {
        RuntimeEnvironment runtime(4, 0, "", "test_files/SimpleArithmeticClass", "add", 0, NULL);
        runtime.run();
    } catch (runtime_error e) {
        testFailed(test_name, "Too small heap size for run");
    }
}

void SimpleArithmeticClassHeapSize2(const char* test_name) {
    cout << TEST_FILE_NAME << " : " << test_name << endl;
    cout << "Insufficient heap size verification" << endl;
    try {
        RuntimeEnvironment runtime(3, 0, "", "test_files/SimpleArithmeticClass", "add", 0, NULL);
        runtime.run();
        testFailed(test_name, "Insufficient heap size for run");
    } catch (runtime_error e) {}
}