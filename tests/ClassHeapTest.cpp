#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include "TestEnv/TestEnv.h"
#include "Constants.h"
#include "DataTypes.h"
#include "ClassHeap.h"

using namespace std;

/*
 * Simple C++ Test Suite.
 */
#define TEST_FILE_NAME "ClassHeap"

void ClassLoad(const char*);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% ClassHeap" << endl;
    cout << "%SUITE_STARTED%" << endl;

    call(ClassLoad, "ClassLoad", TEST_FILE_NAME);

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

void ClassLoad(const char* test_name) {
    try {
        ClassHeap* class_heap = new ClassHeap("examples");
        JavaClass* javaclass = class_heap->getClass("test_files/Child");
        assertNotNull(javaclass, test_name, "Child JavaClass*");
        cout << "Fields count and total count verification" << endl;
        assertEquals((u2)5, javaclass->fields_count, test_name, "Child total fields count");

        cout << "Fields indices verification" << endl;
        assertEquals((u2)0, javaclass->getFieldIndex("parentPublicInt"), test_name, "parentPublicInt field index");
        assertEquals((u2)0, javaclass->getParent()->getFieldIndex("parentPublicInt"), test_name, "parentPublicInt field index");
        assertEquals((u2)1, javaclass->getFieldIndex("parentProtectedShort"), test_name, "parentProtectedShort field index");
        assertEquals((u2)1, javaclass->getParent()->getFieldIndex("parentProtectedShort"), test_name, "parentProtectedShort field index");
        assertEquals((u2)2, javaclass->getParent()->getFieldIndex("parentPrivateChar"), test_name, "parentPrivateChar field index");
        assertEquals((u2)2, javaclass->getFieldIndex("childPublicInt"), test_name, "childPublicInt field index");
        assertEquals((u2)3, javaclass->getFieldIndex("childProtectedShort"), test_name, "childProtectedShort field index");
        assertEquals((u2)4, javaclass->getFieldIndex("childPrivateChar"), test_name, "childPrivateChar field index");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}