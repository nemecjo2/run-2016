#include <cstdlib>
#include <iostream>

#include "Constants.h"
#include "RuntimeEnvironment.h"
#include "TestEnv/TestEnv.h"

using namespace std;

/*
 * Simple C++ Test Suite
 */
#define TEST_FILE_NAME "Static"

void StaticInt(const char *);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% Static" << endl;
    cout << "%SUITE_STARTED%" << endl;

    call(StaticInt, "StaticInt", TEST_FILE_NAME);

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

/* Verification that static integer is shared among all instances. */
void StaticInt(const char * test_name) {
    try {
        RuntimeEnvironment runtime(160, "examples", "test_files/StaticRun", "staticInt", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(111, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}