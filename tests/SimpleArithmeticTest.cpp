#include <cstdlib>
#include <iostream>

#include "Constants.h"
#include "RuntimeEnvironment.h"
#include "TestEnv/TestEnv.h"

using namespace std;

/*
 * Simple C++ Test Suite
 */
#define TEST_FILE_NAME "Arithmetic"

void add(const char *);
void sub(const char *);
void mul(const char *);
void combination(const char *);
void shortSum(const char *);

int main(int argc, char** argv) {
    cout << "%SUITE_STARTING% Arithmetic" << endl;
    cout << "%SUITE_STARTED%" << endl;

    call(add, "Addition", TEST_FILE_NAME);
    call(sub, "Subtraction", TEST_FILE_NAME);
    call(mul, "Multiplication", TEST_FILE_NAME);
    call(combination, "Combination", TEST_FILE_NAME);

    call(shortSum, "ShortSum", TEST_FILE_NAME);

    cout << "%SUITE_FINISHED% time=0" << endl;

    return (EXIT_SUCCESS);
}

void add(const char * test_name) {
    try {
        RuntimeEnvironment runtime(160, "examples", "test_files/SimpleArithmeticClass", "add", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(19, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void sub(const char * test_name) {
    try {
        RuntimeEnvironment runtime(160, "examples", "test_files/SimpleArithmeticClass", "subtract", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(-5, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void mul(const char * test_name) {
    try {
        RuntimeEnvironment runtime(160, "examples", "test_files/SimpleArithmeticClass", "multiply", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(200, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void combination(const char * test_name) {
    try {
        RuntimeEnvironment runtime(160, "examples", "test_files/SimpleArithmeticClass", "strangeSum", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(9, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}

void shortSum(const char * test_name) {
    try {
        RuntimeEnvironment runtime(10, "examples", "test_files/SimpleArithmeticClass", "shortSum", 0, NULL);
        ObjectHandler* result = runtime.run();
        assertNotNull(result, test_name, "Returned ObjectHandler*");
        assertEquals((u1)FIELD_INT, result->type, test_name, "Returned ObjectHandler* type");
        assertEquals(6, result->heap_ptr->_int, test_name, "Returned integer value");
    } catch (runtime_error e) {
        testFailed(test_name, e.what());
    }
}