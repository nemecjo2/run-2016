package overloads;

class B extends A {
  public void foo(X x, Z y) {
	System.out.println("In B::foo(X,Z)");
	x.print();
	y.print();
  }  
}