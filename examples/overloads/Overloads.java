//Jaky bude vystup metody main? Pujde kod kompilovat?

//--------------------------------------------------------------------------------

package overloads;

class Overloads {
  public static void main(String[] args) {
	A a;
	B b;
	X x;
	Y y;
	Z z;
	
	a = new A();
	x = new X();
	y = new Y();
	a.foo(x, y);
	
	a = new B();
	x = new X();
	y = new Z();
	a.foo(x, y);

	b = new B();
	x = new X();
	y = new Z();
	b.foo(x, y);

	b = new B();
	x = new X();
	z = new Z();
	b.foo(x, z);
  }
}