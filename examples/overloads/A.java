package overloads;

class A {
  public void foo(X x, Y y) {
	System.out.println("In A::foo(X,Y)");
	x.print();
	y.print();
  }
}