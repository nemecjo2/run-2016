package files;

public class OurFile {
	private java.io.File file;
	private java.io.FileReader reader;

	public int main() throws java.io.IOException {
		file = new java.io.File("examples/files/File.txt");
		reader = new java.io.FileReader(file);

		System.out.println("Budeme nacitat ze souboru :).");

		int a = reader.read();
		while (a != -1) {
			System.out.print((char) a);
			a = reader.read();
		}
		System.out.println(" ");
		return 0;
	}
}