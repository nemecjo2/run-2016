package garbage_collector;

public class Array {
	public int main() {
		int youngerPeopleCount = 10;
		int youngPeopleCount = 5;
		int startAge = 20;
		int offsetAge = 0;
		Person[] youngerPeople = new Person[youngerPeopleCount];
		for (int i = 0; i < youngerPeopleCount; ++i) {
			youngerPeople[i] = new Person(startAge + i);
		}

		for (int j = 1; j < 2140; ++j) {
			Person[] youngPeople = new Person[youngPeopleCount];
			for (int k = 0; k < youngPeopleCount; ++k) {
				youngPeople[k] = new Person(startAge + offsetAge + k);
			}

			int sum = 0;
			for (int i = 0; i < youngerPeopleCount; ++i) {
				sum += youngerPeople[i].howOldAmI();
			}
			if ( (2 * sum) != ((startAge + (startAge + youngerPeopleCount - 1)) * youngerPeopleCount) )
				return j;
			System.out.println("CHECK 1 -- OK");

			sum = 0;
			for (int i = 0; i < youngPeopleCount; ++i) {
				sum += youngPeople[i].howOldAmI();
			}	
			if ( (2 * sum) != (((startAge + offsetAge) + (startAge + offsetAge + youngPeopleCount - 1)) * youngPeopleCount) )
				return -1 * j; 
			System.out.println("CHECK 2 -- OK");

			++offsetAge;
		}

		return 0;
	}
}