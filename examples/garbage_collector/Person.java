package garbage_collector;

public class Person {
	private int age;

	public Person(int age) {
		this.age = age;
	}

	public int howOldAmI() {
		return age;
	}
}