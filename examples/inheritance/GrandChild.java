package inheritance;

public class GrandChild extends Child {
	private int value;

	public GrandChild(int a, int b, int c) {
		super(b, c);
		value = a;
	}

	public int getNumber() { return value; }

	public int inheritedFieldsTest() {
		return value + protectedChildField + publicParentField + protectedParentField;
	}
}