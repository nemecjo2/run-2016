package inheritance;

public class MainClass {
	public int main() {
		Parent someone = new GrandChild(21, 7, 11);
		System.out.println(someone.getNumber());
		if (someone.getNumber() != 21)
			return 0;

		someone = new Child(7, 11);
		System.out.println(someone.getNumber());
		if (someone.getNumber() != 7)
			return 0;

		someone = new Parent(11);
		System.out.println(someone.getNumber());
		if (someone.getNumber() != 11)
			return 0;

		return someone.getNumber();
	 }

	public int inheritedFields() {
		GrandChild gch = new GrandChild(21, 7, 11);
		// 21 + 37 + 12 + 22 = 92
		return gch.inheritedFieldsTest();
	}
}