package inheritance;

public class Child extends Parent {
	private int value;
	protected int protectedChildField;

	public Child(int value, int value2) {
		super(value2);
		this.value = value;
		protectedChildField = this.value + 30;
	}

	public int getNumber() { return value; }
}