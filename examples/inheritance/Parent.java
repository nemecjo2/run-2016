package inheritance;

public class Parent {
	private int value;
	public int publicParentField;
	protected int protectedParentField;

	public Parent(int a) {
		value = a;
		publicParentField = value + 1;
		protectedParentField = publicParentField + 10;
	}

	public int getNumber() { return value; }
}