package satsolver;

public class Clause {
    private final Element[] elements;
    private int addIndex;

    public Clause(int numberOfElements) {
        elements = new Element[numberOfElements];
        addIndex = 0;
    }    

    public void addLiteral(Literal literal, boolean sign) {
        elements[addIndex++] = new Element( literal, sign);
    }
    
    public boolean evaluate() {
        for( Element element : elements ) {
            if( element.getValue()  ) { // One element is good enough to set whole clause true.
                return true;
            }
        }
        
        return false;
    }
}