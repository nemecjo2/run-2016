package satsolver;

public class Literal {
    protected boolean value;

    public Literal() {
        this.value = false;
    }

    public void setTrue() {
        this.value = true;
    }

    public void setFalse() {
        this.value = false;
    }

    public void negate() {
        this.value = !this.value;
    }

    public boolean getValue() {
        return this.value;
    }

    public int toInt() {
        return (this.value ? 1 : 0);
    }
}