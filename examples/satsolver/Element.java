package satsolver;
    
public class Element {
    protected Literal literal;
    protected boolean sign;

    public Element(Literal literal, boolean sign) {
        this.literal = literal;
        this.sign = sign;
    }

    public Literal getLiteral() {
        return literal;
    }

    public boolean isPositive() {
        return sign;
    }
    
    public boolean getValue() {
        if( !sign ) {
            return !literal.getValue();
        } else {
            return literal.getValue();
        }
    }
}