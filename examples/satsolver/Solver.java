package satsolver;

import java.io.FileReader;
import java.io.IOException;

public class Solver {
    public Formula formula;
    public Literal[] literals;
    private static final int CR = 13;

    public void createSomething() {
        int numberOfClauses = 2;
        int numberOfLiterals = 3;
        formula = new Formula(numberOfClauses);
        literals = new Literal[numberOfLiterals];
        for (int i = 0; i < numberOfLiterals; i++) {
            literals[i] = new Literal();
        }

        Clause clause = new Clause(2);
        clause.addLiteral(literals[0], true);
        clause.addLiteral(literals[1], false);
        formula.addClause(clause);

        clause = new Clause(3);
        clause.addLiteral(literals[0], false);
        clause.addLiteral(literals[1], true);
        clause.addLiteral(literals[2], false);
        formula.addClause(clause);
    }
    
    public void fetchFileContent(FileReader reader) throws IOException {
       // Read number of literals and clauses.
        int numberOfLiterals, numberOfClauses;
        numberOfLiterals = (reader.read() - '0');
        // Skip SPACE
        reader.read();
        numberOfClauses = (reader.read() - '0');
        // Skip CRLF
        reader.read();

        // Create formula with given number of clauses.
        formula = new Formula(numberOfClauses);

        // Create literals.
        literals = new Literal[numberOfLiterals];
        for (int i = 0; i < numberOfLiterals; i++) {
            literals[i] = new Literal();
        }

        // Create clauses
        Clause clause;
        int literalClauses;
        boolean currentSign = true;
        int currentLiteral = 0;
        for (int i = 0; i < numberOfClauses; i++) {
            System.out.print("Clause: ");
            System.out.println(i);
            literalClauses = (reader.read() - '0');
            // Skip CRLF
            reader.read();
            clause = new Clause(literalClauses);
            int read_i = reader.read();
            while (read_i != '\n') {
                if (read_i == '-') {
                    currentSign = false;
                } else if (read_i == ' ') {
                    clause.addLiteral(literals[currentLiteral-1], currentSign);
                    System.out.print("Adding literal ");
                    System.out.print(currentLiteral);
                    System.out.print(" to clause ");
                    System.out.print(i);
                    System.out.print(" sign: ");
                    System.out.println(boolToInt(currentSign));
                    currentSign = true;
                } else {
                    currentLiteral = (read_i - '0');
                }
                read_i = reader.read();
            }
            clause.addLiteral(literals[currentLiteral-1], currentSign);
            System.out.print("Adding literal ");
            System.out.print(currentLiteral);
            System.out.print(" to clause ");
            System.out.print(i);
            System.out.print(" sign: ");
            System.out.println(boolToInt(currentSign));
            formula.addClause(clause);
        }
    }

    protected int boolToInt(boolean value) {
        return value ? 1 : 0;
    }

    public boolean solve() {
        return entryLevel(literals.length - 1);
    }

    public Literal[] getConfiguration() {
        return literals;
    }

    protected boolean entryLevel(int level) {
        if (level < 0) {
            return formula.evaluate();
        } else {
            if (entryLevel(level-1)) {
                return true;
            }
            literals[level].negate();
            if (entryLevel(level-1)) {
                return true;
            }
        }

        return false;
    }
}