package satsolver;

public class Formula {
    protected Clause[] clauses;
    protected int addIndex;

    public Formula(int numberOfClauses) {
        clauses = new Clause[numberOfClauses];
        addIndex = 0;
    }

    public void addClause(Clause clause ) {
        clauses[addIndex++] = clause;
    }

    public boolean evaluate() {
        for(Clause clause: clauses) {
            if (!clause.evaluate()) { // All clauses need to be true to satisfy formula.
                return false;
            }
        }

        return true;
    }
    
}