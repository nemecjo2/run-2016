package satsolver;

import java.io.IOException;

public class SATSolver {
    //public static void main(String[] args) {
    public void main() throws java.io.FileNotFoundException, java.io.IOException {
        java.io.File file = new java.io.File("examples/satsolver/02.sat");
        java.io.FileReader reader = new java.io.FileReader(file);
        Solver solver = new Solver();
        System.out.println("Reading input:");
        solver.fetchFileContent(reader);

        // Brute force approach -- eval each (break on the first good).
        boolean result = solver.solve();
        Literal[] literals;

        System.out.print("\nResult: ");
        if (result) {
            System.out.println("TRUE");
            literals = solver.getConfiguration();

            System.out.print("[");
            for (int i = 0; i < literals.length; i++) {
                if (i != 0) {
                    System.out.print(", ");
                }
                System.out.print(literals[i].toInt());
            }
            System.out.println("]");
        } else {
            System.out.println("FALSE");
        }

    }
}