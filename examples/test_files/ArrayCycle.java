package cz.cvut.fit.run;

public class ArrayCycle {
    public int main() {
        int[] pole = new int[20];
        for(int i=0; i<20; i++) {
            pole[i] = i+1;
        }
        int soucet = 0;
        for (int j=0; j<20; j++) {
            for (int g=0; g<20; g++) {
                soucet += pole[g];
            }
            soucet += j+1;
        }
        System.out.println(soucet);
        return 0;
    }
}