package test_files;

public class SimpleArithmeticClass {
    public int multiply() {
        int i = 10;
        int j = 20;
        int k = j*i;
        return k;
    }
    public int add() {
        int i = 13;
        int j = 6;
        int k = j+i;
        return k;
    }
    public int subtract() {
        int i = 14;
        int j = 9;
        int k = j-i;
        return k;
    }

    public int strangeSum() {
        int x = 3;
        int y = 6;
        int pom = x;
        x = y;
        y = pom;

        y += 1;
        x = x - 1;

        return x + y;
    }

    public int shortSum() {
        short a = -14;
        short b = 20;
        return a + b;
    }
}