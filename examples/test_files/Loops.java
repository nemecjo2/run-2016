package test_files;

public class Loops {
    public int theSimplestForIntLoop() {
        for (int i = 0; i < 20; ++i)
            i = i + 2;
        return 1;
    }

    public int simpleForIntLoop() {
        int a = 30;
        for (int i = 0; i < 20; ++i)
            --a;
        return a;
    }

    public int simpleWhileIntLoop() {
        int a = 0;
        while (a < 3)
            a++;
        return a;
    }
}