/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.ddw.pagerank;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author kevin
 */
public class SimpleClass {
    private int integerField;

    public String stringField;

    private static final char charField = 'a';

    protected void voidMethod() {
        integerField = 5;
    }

    public Iterator<String> iterator() {
        ArrayList<String> str = new ArrayList<>();
        str.add("prvni");
        str.add("druhy");
        return str.iterator();
    }

    public SimpleClass(int integerField, String stringField) {
        this.integerField = integerField;
        this.stringField = stringField;
    }
}
