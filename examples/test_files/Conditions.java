package test_files;

public class Conditions {
    public int positiveIntCondition() {
        int a = 1;
        if (a == 1)
            a = 21;
        return a;
    }

    public int negativelyPositiveIntCondition() {
        int a = 3;
        if (a == 4)
            a = 5;
        return a;
    }

     public int negativeIntCondition() {
        int a = 2;
        if (a != 1)
            a = 4;
        return a;
    }

    public int negativelyNegativeIntCondition() {
        int a = 4;
        if (a != 4)
            a = 52;
        return a;
    }

    public int lessIntCondition() {
        int a = 14;
        if (a < 15)
            a = 152;
        return a;
    }

    public int negativeLessIntCondition() {
        int a = 1881;
        if (a < 15)
            a = 152;
        return a;
    }

    public int lessOrEqualIntCondition() {
        int a = 15;
        if (a <= 15)
            a = 155;
        return a;
    }

    public int negativeLessOrEqualIntCondition() {
        int a = 16;
        if (a <= 15)
            a = 1221;
        return a;
    }

    public int greaterIntCondition() {
        int a = 24;
        if (a > 15)
            a = 11;
        return a;
    }

    public int negativeGreaterIntCondition() {
        int a = 11;
        if (a > 15)
            a = 8973;
        return a;
    }

    public int greaterOrEqualIntCondition() {
        int a = 15;
        if (a >= 15)
            a = 200;
        return a;
    }

    public int negativeGreaterOrEqualIntCondition() {
        int a = 14;
        if (a >= 15)
            a = 20;
        return a;
    }

    public int intCondition() {
        int b = 20;
        int a = 11;
        if (b == 21)
            a = 92;
        else
            a++;

        return a;
    }

    public int positiveBoolCondition() {
        boolean b = true;
        int a = 21;
        if (b == true)
            --a;
        else
            ++a;

        return a;
    }

    public int negativelyPositiveBoolCondition() {
        boolean b = false;
        int a = 34;
        if (b == true)
            --a;
        else
            ++a;

        return a;
    }

    public int negativeBoolCondition() {
        boolean b = true;
        int a = 14;
        if (b != false)
            --a;
        else
            ++a;

        return a;
    }

    public int negativelyNegativeBoolCondition() {
        boolean b = 11 == 11;
        int a = 71;
        if (b != true)
            --a;
        else
            ++a;

        return a;
    }

    public int positiveZeroCondition() {
        int a = 0;
        if (a == 0)
            --a;
        else
            ++a;
        return a;
    }

    public int negativelyPositiveZeroCondition() {
        int b = 11;
        if (b == 0)
            --b;
        else
            ++b;
        return b;
    }

    public int negativeZeroCondition() {
        int a = 37;
        if (a != 0)
            --a;
        else
            ++a;
        return a;
    }

    public int negativelyNegativeZeroCondition() {
        int b = 0;
        if (b != 0)
            --b;
        else
            ++b;
        return b;
    }

    public int lessZeroCondition() {
        int a = -20;
        if (a < 0)
            a = 154;
        else
            a = 231;
        return a;
    }

    public int negativeLessZeroCondition() {
        int a = 1881;
        if (a < 0)
            a = 152;
        else
            ++a;
        return a;
    }

    public int lessOrEqualZeroCondition() {
        int a = 0;
        int b = -100;
        if (a <= 0 && b <= 0)
            a = 2012;
        else
            a--;
        return a;
    }

    public int negativeLessOrEqualZeroCondition() {
        int a = 18;
        if (a <= 0)
            a = 1221;
        return a;
    }

    public int greaterZeroCondition() {
        int a = 24;
        if (a > 0)
            a += 11;
        else
            --a;
        return a;
    }

    public int negativeGreaterZeroCondition() {
        int a = 11;
        if (a > 15)
            a -= 2;
        else
            a -= 3;
        return a;
    }

    public int greaterOrEqualZeroCondition() {
        int a = 15;
        int b = 0;
        if (a >= 0 && b >= 0)
            a = 202;
        else
            a = 300;
        return a;
    }

    public int negativeGreaterOrEqualZeroCondition() {
        int a = -14;
        if (a >= 0)
            a = 20;
        return a;
    }
}