package test_files;

public class MethodCall {
    public int main() {
        int x = getX();
        int y = getY();
        return x+y;
    }
    public int methodArguments() {
        return sum(getX(), getY());
    }
    public int getX() {
        int x = 12;
        return x + getY();
    }
    public int getY() {
        return 15;
    }
    public Object getNull() {
        return null;
    }
    public int sum(int a, int b) {
        return a+b;
    }
}