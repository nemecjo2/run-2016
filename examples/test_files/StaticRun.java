package test_files;

public class StaticRun {
  private StaticInt f11;

    public int staticInt() {
        f11 = new StaticInt();
        f11.staticInt = 5;

        StaticInt f2 = new StaticInt();
        f2.staticInt++;
        f2.staticInt++;
        f2.staticInt++;

        StaticInt f3 = new StaticInt();
        f3.staticInt--;

        StaticInt.staticInt += 104;
        return StaticInt.staticInt;
    }

    public int staticObject() {
        StaticObject f1 = new StaticObject();
        f1.myObject = new MyObject();
        f1.myObject.myInteger = 5;

        StaticObject f2 = new StaticObject();
        f2.myObject.myInteger++;
        f2.myObject.myInteger++;
        f2.myObject.myInteger++;

        StaticObject f3 = new StaticObject();
        f3.myObject.myInteger--;

        StaticObject.myObject.myInteger += 104;
        return StaticObject.myObject.myInteger;
    }
}

class StaticInt {
	public static int staticInt = 1;
}

class StaticObject {
	public static MyObject myObject = new MyObject();
}

class MyObject {
	public int myInteger = 5;
}