package test_files;

public class Parent {
    public int parentPublicInt;
    protected short parentProtectedShort;
    private char parentPrivateChar;

    public int parentPublic() {
        return 5;
    }
    protected int parentProtected() {
        return 7;
    }
    private int parentPrivate() {
        return 9;
    }

    protected int inheritedMethod() {
        return 5;//"Parent inherited method :-/.";
    }
}