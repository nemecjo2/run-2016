package test_files;

public class ArrayTest {
    public int getSet() {
        int array[] = new int[7];
        array[3] = 2;
        int a = array[3];
        return a;
    }
    public int defaultValue() {
        int array[] = new int[7];
        int a = array[3];
        return a;
    }
    public int arithmetic() {
        int array[] = new int[7];
        array[1] = 2;
        array[2] = 1;
        array[3] = 4;
        array[0] = array[1] + array[2];
        array[0] += array[3];
        int a = array[0];
        return a;
    }
    public int arraySize() {
        int array[] = new int[5];
        return array.length;
    }
}