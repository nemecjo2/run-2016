package test_files;

public class Child extends Parent {
    public int childPublicInt;
    protected short childProtectedShort;
    private char childPrivateChar;

    public int childPublic() {
        return 55;
    }
    protected int childProtected() {
        return 77;
    }
    private int childPrivate() {
        return 99;
    }
    private int callChild() {
        return childPrivate() + childPublic() + childProtected();
    }
    private int callParent() {
        return parentProtected() + parentPublic();
    }
    protected int parentProtected() {
        return 20;
    }

    protected int inheritedMethod() {
        return 33;//"Child inherited method :-).";
    }
    private void inheritanceTest() {
        Parent instance = new Child();
        System.out.println(instance.inheritedMethod());
    }
}