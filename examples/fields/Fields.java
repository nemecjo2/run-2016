package cz.cvut.fit.run;
public class Fields {

    private int intField;
    private char charField = '!';
    private short shortField;

    public void setIntField() {
        intField = 19;
    }
    public int returnIntField() {
        setIntField();
        return intField;
    }
    public int returnIntFieldDirectly() {
        intField = 21;
        return intField;
    }
    public char returnCharField() {
        charField++;
        return charField;
    }
    public short returnShortField() {
        return shortField;
    }
}