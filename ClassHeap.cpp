#include "ClassHeap.h"

#include <cstring>
#include <stdexcept>

ClassHeap::ClassHeap(const char* classes_path) : classes_path(classes_path) {}

ClassHeap::~ClassHeap() {
}

///////////////////////////////////////////////////////
// Public methods
bool ClassHeap::addClass(JavaClass* javaclass) {
    if (javaclass == NULL)
        return false;

    string class_name = javaclass->getName();

    // Check if already exists.
    if (class_map.find(class_name) == class_map.end())
        class_map[class_name] = javaclass;
    return true;
}

JavaClass* ClassHeap::getClass(string class_name) {
    // Check if already exists.
    if (class_map.find(class_name) == class_map.end()) {
        class_map[class_name] = loadClass(class_name);
    }

    return class_map.at(class_name);
}

void ClassHeap::getIterator(map <string, JavaClass*>::iterator& iterator, map <string, JavaClass*>::iterator& end) {
    iterator = class_map.begin();
    end = class_map.end();
}

///////////////////////////////////////////////////////
// Private methods
JavaClass* ClassHeap::loadClass(string class_name) {
    FILE* file;
    string class_file(classes_path);
    if (strcmp(classes_path, "") != 0)
    class_file.append("/");
    class_file.append(class_name).append(".class");

    #ifdef DEBUG
        cout << "Reading file: " << class_file << endl;
    #endif

    file = fopen(class_file.data(), "rb");
    if (file == NULL) {
        class_file = "lib/";
        class_file.append(class_name).append(".class");
        file = fopen(class_file.data(), "rb");

        if (file == NULL) {
            string message("Class ");
            message.append(class_name).append(" could not be loaded (").append(class_name).append(".class not found)");
            throw runtime_error(message);
        }
    }

    JavaClass* javaclass = new JavaClass(this);
    javaclass->fetch(file);
    fclose(file);

    return javaclass;
}