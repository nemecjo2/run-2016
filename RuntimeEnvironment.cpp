#include <cstring>

#include "RuntimeEnvironment.h"
#include "InstructionSet.h"

RuntimeEnvironment::RuntimeEnvironment(uint32_t heap_size, const char* classes_path, const char* class_name, const char* main_method_name, int argc, char** argv)
    : class_heap(classes_path), object_heap(heap_size, &frame_stack, &class_heap, &open_files) {

    class_heap.object_heap = &object_heap;

    // Create Main Class definition.
    JavaClass* main_class = class_heap.getClass(string(class_name));

    // Make alias for full path name.
    class_heap.addClass(main_class);

    // Create instance of main_class on Object_heap (if not static ?)
    MethodItem* method = main_class->getMethod(main_method_name);
    ObjectHandler* handler = object_heap.createObjectHandler(main_class);
    frame_stack.createFrame(method, handler);

    // TODO Push arguments on stack of actual frame.
}

RuntimeEnvironment::~RuntimeEnvironment() {
}

ObjectHandler* RuntimeEnvironment::run() {
    return_value = NULL;
    while (!frame_stack.isEmpty()) {
        actual_frame = frame_stack.getActualFrame();
        u1 operation = actual_frame->getNext();

        #ifdef STACK_DUMP
            stack_dump();
        #endif
        #ifdef DEBUG
            printf("PC: %d | %x\n", actual_frame->pc - 1, operation & 0xff); // PC was incremented by actual_frame->getNext().
        #endif

        switch (operation) {
            case nop: {
                break;
            }
            case aconst_null: {
                aconst_nullCase();
                break;
            }
            case _goto: {
                gotoCase();
                break;
            }
            case _return: {
                returnVoidCase();
                break;
            }
            case areturn:
            case ireturn: {
                returnValueCase();
                break;
            }
            case _new: {
                newCase();
                break;
            }
            case newarray: {
                newArrayCase();
                break;
            }
            case anewarray: {
                anewArrayCase();
                break;
            }
            case arraylength: {
                arrayLengthCase();
                break;
            }
            case getstatic:
            case getfield: {
                getFieldCase(operation);
                break;
            }
            case putstatic:
            case putfield: {
                putFieldCase(operation);
                break;
            }
            case pop: {
                popCase();
                break;
            }
            case dup: {
                dupCase();
                break;
            }
            case dup_x1: {
                dupx1Case();
                break;
            }
            case dup2: {
                dup2Case();
                break;
            }
            case i2c: {
                i2cCase();
                break;
            }
            case iconst_m1:
            case iconst_0:
            case iconst_1:
            case iconst_2:
            case iconst_3:
            case iconst_4:
            case iconst_5: {
                iconstCase(operation);
                break;
            }
            case iastore: {
                iastoreCase();
                break;
            }
            case istore:
            case astore: {
                istoreCase();
                break;
            }
            case aastore: {
                aastoreCase();
                break;
            }
            case astore_0:
            case astore_1:
            case astore_2:
            case astore_3: {
                astore_nCase(operation);
                break;
            }
            case istore_0:
            case istore_1:
            case istore_2:
            case istore_3: {
                istore_nCase(operation);
                break;
            }
            case iaload: {
                ialoadCase();
                break;
            }
            case aload:
            case iload: {
                iloadCase();
                break;
            }
            case aaload: {
                aaloadCase();
                break;
            }
            case aload_0:
            case aload_1:
            case aload_2:
            case aload_3: {
                aload_nCase(operation);
                break;
            }
            case iload_0:
            case iload_1:
            case iload_2:
            case iload_3: {
                iload_nCase(operation);
                break;
            }
            case iadd: {
                iaddCase();
                break;
            }
            case isub: {
                isubCase();
                break;
            }
            case imul: {
                imulCase();
                break;
            }
            case iinc: {
                iincCase();
                break;
            }
            case ior: {
                iorCase();
                break;
            }
            case iand: {
                iandCase();
                break;
            }
            case bipush: {
                bipushCase();
                break;
            }
            case sipush: {
                sipushCase();
                break;
            }
            case ldc: {
                ldcCase();
                break;
            }
            case invokevirtual: {
                invokevirtualCase();
                break;
            }
            case invokespecial: {
                invokespecialCase();
                break;
            }
            case ifeq:
            case ifne:
            case iflt:
            case ifge:
            case ifgt:
            case ifle: {
                ifCase(operation);
                break;
            }            
            case if_icmpeq:
            case if_icmpne:
            case if_icmplt:
            case if_icmpge:
            case if_icmpgt:            
            case if_icmple: {
                if_icmpCase(operation);
                break;
            }
            default: {
                printf("Undefined operation: %x\n", operation & 0xff);
                throw runtime_error("Requested operation is not implemented.");
            }
        }
    }
    return return_value;
}

void RuntimeEnvironment::aconst_nullCase() {
    actual_frame->pushOnStack(NULL);
}

void RuntimeEnvironment::newCase() {
    // Get Class index to Constant Pool.
    u1 byte1 = actual_frame->getNext();
    u1 byte2 = actual_frame->getNext();
    u2 index = (byte1 << 8) | byte2;

    // Get current class definition.
    JavaClass* class_definition = actual_frame->getMethodOwnerClass();

    // Get class of object to be created.
    char* class_name;
    ClassConstantPoolItem* class_item = (ClassConstantPoolItem*) class_definition->constant_pool[index];
    class_definition->getStringFromConstantPool(class_item->name_index, class_name);
    JavaClass* javaclass = class_heap.getClass(string(class_name));

    // Create object.
    ObjectHandler* created_object = object_heap.createObjectHandler(javaclass);
    // Push object on stack.
    actual_frame->pushOnStack(created_object);
}

void RuntimeEnvironment::gotoCase() {
    u1 branchbyte1 = actual_frame->getNext();
    u1 branchbyte2 = actual_frame->getNext();
    s2 offset = (branchbyte1 << 8) | branchbyte2;
    offset -= 3; // Execution then proceeds at that offset from the address of the opcode of this goto instruction. 2 branch bytes were already readed.
    actual_frame->pc += offset;    
}

void RuntimeEnvironment::returnVoidCase() {
    #ifdef DEBUG
        cout << "Returning from method." << endl;
    #endif
    frame_stack.popActualFrame();
}

void RuntimeEnvironment::returnValueCase() {
    // Pop value from stack.
    return_value = actual_frame->popFromStack();
    #ifdef DEBUG
        cout << "Return from method " << actual_frame->getMethodName() << endl;
        cout << "\tINT: " << return_value->heap_ptr->_int << endl;
    #endif
    // Pop actual frame.
    frame_stack.popActualFrame();
    if (!frame_stack.isEmpty()) {
        // Push return integer value on stack of previous frame.
        frame_stack.getActualFrame()->pushOnStack(return_value);
    }
}

void RuntimeEnvironment::newArrayCase() {
    // Get requested array size.
    ObjectHandler* size = actual_frame->popFromStack();
    // Get array type.
    u1 type = actual_frame->getNext();
    // Create handler for array.
    ObjectHandler* array = object_heap.createArrayHandler(size->heap_ptr->_int, type);
    // Push array on stack.
    actual_frame->pushOnStack(array);
}

void RuntimeEnvironment::anewArrayCase() {
    // Get requested array size.
    ObjectHandler* size = actual_frame->popFromStack();
    // Get Classref index to Constant Pool.
    u1 byte1 = actual_frame->getNext();
    u1 byte2 = actual_frame->getNext();
    u2 index = (byte1 << 8) | byte2;

    // Get JavaClass for Class of array to be created.
    ClassConstantPoolItem* class_item = (ClassConstantPoolItem*) actual_frame->getMethodOwnerClass()->constant_pool[index];
    char* class_name;
    actual_frame->getMethodOwnerClass()->getStringFromConstantPool(class_item->name_index, class_name);

    JavaClass* javaclass = class_heap.getClass(string(class_name));
    if (javaclass->hasMethod("<clinit>") && javaclass->initialized == false) {
        actual_frame->pc -= 3;
        actual_frame->pushOnStack(size);
        clinit(javaclass);
        return;
    }

    // Create handler for array.
    ObjectHandler* array = object_heap.createArrayHandler(size->heap_ptr->_int, javaclass);
    // Push array on stack.
    actual_frame->pushOnStack(array);
}

void RuntimeEnvironment::arrayLengthCase() {
    // Get array from stack.
    ObjectHandler* array = actual_frame->popFromStack();
    // Create handler for array size.
    ObjectHandler* size = object_heap.createIntHandler(array->heap_ptr->_int);
    // Push array size on stack.
    actual_frame->pushOnStack(size);
}

void RuntimeEnvironment::getFieldCase(u1 operation) {
    // Get Fieldref index to Constant Pool.
    u1 byte1 = actual_frame->getNext();
    u1 byte2 = actual_frame->getNext();
    u2 index = (byte1 << 8) | byte2;

    // Current class and run-time constant pool.
    JavaClass* current_class = frame_stack.getActualFrame()->getMethodOwnerClass();
    ConstantPoolItem** constant_pool = current_class->constant_pool;
    FieldrefConstantPoolItem* field = (FieldrefConstantPoolItem*) constant_pool[index];
    NameAndTypeConstantPoolItem* name_type = (NameAndTypeConstantPoolItem*) constant_pool[field->name_and_type_index];
    char* field_name;
    frame_stack.getActualFrame()->getMethodOwnerClass()->getStringFromConstantPool(name_type->name_index, field_name);
    char* descriptor;
    frame_stack.getActualFrame()->getMethodOwnerClass()->getStringFromConstantPool(name_type->descriptor_index, descriptor);

    // Get class index and class reference.
    ClassConstantPoolItem* class_ref = (ClassConstantPoolItem*) constant_pool[field->class_index];
    char* class_name;
    current_class->getStringFromConstantPool(class_ref->name_index, class_name);
    JavaClass* javaclass = class_heap.getClass(class_name);

    ObjectHandler* value;
    if (operation == getfield) {
        ObjectHandler* object = actual_frame->popFromStack();
        if (object->type != FIELD_CLASS)
            throw runtime_error("Get Field instrunction hasn't handler of OBJECT type.");

        int field_index = object->class_definition->getFieldIndex(field_name);

        // Descriptor switch.
        if (descriptor[0] == FIELD_CLASS || descriptor[0] == FIELD_ARRAY)
            value = (ObjectHandler*)((object->heap_ptr+1+field_index)->_ptr); // TODO Is it enough?
        else if (descriptor[0] == FIELD_INT)
            value = object_heap.createIntHandler((object->heap_ptr+1+field_index)->_int);
        else if (descriptor[0] == FIELD_BOOL)
            value = object_heap.createBoolHandler((object->heap_ptr+1+field_index)->_char);
        else {
            string message("Get Field instruction can not handle type: ");
            message.append(descriptor).append(" .");
            throw runtime_error(message);
        }
    }
    else if (operation == getstatic) {
        if (javaclass->initialized == false) {
            actual_frame->pc -= 3;

            clinit(javaclass);
            return;
        }

        int field_index = javaclass->getFieldIndex(field_name);
        value = javaclass->fields[field_index]->value;
    }

    // Push on stack Field value.
    actual_frame->pushOnStack(value);
}

void RuntimeEnvironment::putFieldCase(u1 operation) {
    // Get Fieldref index to Constant Pool.
    u1 byte1 = actual_frame->getNext();
    u1 byte2 = actual_frame->getNext();
    u2 index = (byte1 << 8) | byte2;
    // Get Value to be set.
    ObjectHandler* value = actual_frame->popFromStack();
     
    // Current class and run-time constant pool.
    JavaClass* current_class = frame_stack.getActualFrame()->getMethodOwnerClass();
    ConstantPoolItem** constant_pool = current_class->constant_pool;
    FieldrefConstantPoolItem* field = (FieldrefConstantPoolItem*) constant_pool[index];
    NameAndTypeConstantPoolItem* name_type = (NameAndTypeConstantPoolItem*) constant_pool[field->name_and_type_index];
    char* field_name;
    frame_stack.getActualFrame()->getMethodOwnerClass()->getStringFromConstantPool(name_type->name_index, field_name);
    char* descriptor;
    frame_stack.getActualFrame()->getMethodOwnerClass()->getStringFromConstantPool(name_type->descriptor_index, descriptor);

    // Get class index and class reference.
    ClassConstantPoolItem* class_ref = (ClassConstantPoolItem*) constant_pool[field->class_index];
    char* class_name;
    current_class->getStringFromConstantPool(class_ref->name_index, class_name);
    JavaClass* javaclass = class_heap.getClass(class_name);

    // Set Field value.
    if (operation == putfield) {
        ObjectHandler* object = actual_frame->popFromStack();
        if (object->type != FIELD_CLASS)
            throw runtime_error("Get Field instrunction hasn't handler of OBJECT type.");

        int field_index = object->class_definition->getFieldIndex(field_name);

        // Descriptor switch.
        if (descriptor[0] == FIELD_CLASS || descriptor[0] == FIELD_ARRAY)
            (object->heap_ptr+1+field_index)->_ptr = value;
        else if (descriptor[0] == FIELD_INT)
            (object->heap_ptr+1+field_index)->_int = value->heap_ptr->_int;
        else if (descriptor[0] == FIELD_BOOL)
            (object->heap_ptr+1+field_index)->_char = value->heap_ptr->_char;
        else {
            string message("Put Field instruction can not handle type: ");
            message.append(descriptor).append(".");
            throw runtime_error(message);
        }
    }
    else if (operation == putstatic) {
        if (javaclass->initialized == false) {
            actual_frame->pc -= 3;
            actual_frame->pushOnStack(value);

            clinit(javaclass);
            return;
        }

        // Set value to Class definition.
        int field_index = javaclass->getFieldIndex(field_name);
        javaclass->fields[field_index]->value = value;
    }
}

void RuntimeEnvironment::popCase() {
    // Pop value from stack.
    actual_frame->popFromStack();
}

void RuntimeEnvironment::dupCase() {
    // Pop value from stack.
    ObjectHandler* value = actual_frame->popFromStack();
    // Push it on stack twice.
    actual_frame->pushOnStack(value);
    actual_frame->pushOnStack(value);
}

void RuntimeEnvironment::dupx1Case() {
    // Pop values from stack.
    ObjectHandler* value1 = actual_frame->popFromStack();
    ObjectHandler* value2 = actual_frame->popFromStack();
    // Push it on stack as required.
    actual_frame->pushOnStack(value1);
    actual_frame->pushOnStack(value2);
    actual_frame->pushOnStack(value1);
}

void RuntimeEnvironment::dup2Case() {
    if (actual_frame->getStackSize() == 1) {
        dupCase();
    } else {
        // Pop values from stack.
        ObjectHandler* value1 = actual_frame->popFromStack();
        ObjectHandler* value2 = actual_frame->popFromStack();
        // Push it on stack twice.
        actual_frame->pushOnStack(value2);
        actual_frame->pushOnStack(value1);
        actual_frame->pushOnStack(value2);
        actual_frame->pushOnStack(value1);
    }
}

void RuntimeEnvironment::i2cCase() {
    ObjectHandler* value = actual_frame->popFromStack();
    value->heap_ptr->_char &= 0x00FF;
    value->type = FIELD_CHAR;
    actual_frame->pushOnStack(value);
}

void RuntimeEnvironment::aload_nCase(u1 operation) {
    // Get local variable at constant index.
    ObjectHandler* value = actual_frame->getLocalVariable((int)(operation - aload_0));
    // Push object value on stack.
    actual_frame->pushOnStack(value);
}

void RuntimeEnvironment::iastoreCase() {
    // Get integer value from stack.
    ObjectHandler* value = actual_frame->popFromStack();
    // Get index from stack.
    ObjectHandler* index = actual_frame->popFromStack();
    // Get array value from stack.
    ObjectHandler* array = actual_frame->popFromStack();
    // Store value to array at index (+2).
    (array->heap_ptr+2+index->heap_ptr->_int)->_int = value->heap_ptr->_int;
}

void RuntimeEnvironment::aastoreCase() {
    // Get integer value from stack.
    ObjectHandler* value = actual_frame->popFromStack();
    // Get index from stack.
    ObjectHandler* index = actual_frame->popFromStack();
    // Get array value from stack.
    ObjectHandler* array = actual_frame->popFromStack();
    // Store value to array at index (+2).
    (array->heap_ptr+2+index->heap_ptr->_int)->_ptr = (void*) value;
}

void RuntimeEnvironment::iconstCase(u1 operation) {
    // Create integer value for constant.
    ObjectHandler* constant = object_heap.createIntHandler((s4)(operation - iconst_0));
    // Push integer value on stack.
    actual_frame->pushOnStack(constant);
}

void RuntimeEnvironment::astore_nCase(u1 operation) {
    // Store object value from stack to local variable.
    actual_frame->setLocalVariable((int)(operation - astore_0), actual_frame->popFromStack());
}

void RuntimeEnvironment::istore_nCase(u1 operation) {
    // Store integer value from stack to local variable.
    actual_frame->setLocalVariable((int)(operation - istore_0), actual_frame->popFromStack());
}

void RuntimeEnvironment::istoreCase() {
    u1 index = actual_frame->getNext();
    // Pop integer value from stack.
    ObjectHandler* value = actual_frame->popFromStack();
    // Store value to local variable at given index.
    actual_frame->setLocalVariable(index, value);
}

void RuntimeEnvironment::ialoadCase() {
    // Pop integer value from stack.
    ObjectHandler* index = actual_frame->popFromStack();
    // Pop object value from stack.
    ObjectHandler* array = actual_frame->popFromStack();
    // Get integer value from array at index.
    s4 value = (array->heap_ptr+2+index->heap_ptr->_int)->_int;
    // Create integer value on Object heap.
    ObjectHandler* integer_value = object_heap.createIntHandler(value);
    // Push integer on stack.
    actual_frame->pushOnStack(integer_value);
}

void RuntimeEnvironment::iload_nCase(u1 operation) {
    // Push integer value on stack from local variable.
    actual_frame->pushOnStack(actual_frame->getLocalVariable((int)(operation - iload_0)));
}

void RuntimeEnvironment::iloadCase() {
    u1 index = actual_frame->getNext();
    // Get local variable at given index.
    ObjectHandler* value = actual_frame->getLocalVariable(index);
    // Push integer value on stack.
    actual_frame->pushOnStack(value);
}

void RuntimeEnvironment::aaloadCase() {
    // Get Index to array.
    ObjectHandler* index = actual_frame->popFromStack();
    // Get Array object.
    ObjectHandler* array = actual_frame->popFromStack();
    // Get Value of Array on given Index.
    ObjectHandler* value = (ObjectHandler*)((array->heap_ptr+2+index->heap_ptr->_int)->_ptr);
    // Push Value on stack.
    actual_frame->pushOnStack(value);
}

void RuntimeEnvironment::iaddCase() {
    // Pop int value from operand stack.
    ObjectHandler* val1 = actual_frame->popFromStack();
    // Pop int value from operand stack.
    ObjectHandler* val2 = actual_frame->popFromStack();
    // Sum values.
    s4 sum = val1->heap_ptr->_int + val2->heap_ptr->_int;
    // Push result on operand stack.
    ObjectHandler* result = object_heap.createIntHandler(sum);
    actual_frame->pushOnStack(result);
}

void RuntimeEnvironment::isubCase() {
    // Pop int value from operand stack.
    ObjectHandler* val1 = actual_frame->popFromStack();
    // Pop int value from operand stack.
    ObjectHandler* val2 = actual_frame->popFromStack();
    // Subtract values.
    s4 sub = val2->heap_ptr->_int - val1->heap_ptr->_int;
    // Push result on operand stack.
    ObjectHandler* result = object_heap.createIntHandler(sub);
    actual_frame->pushOnStack(result);
}

void RuntimeEnvironment::imulCase() {
    // Pop int value from operand stack.
    ObjectHandler* val1 = actual_frame->popFromStack();
    // Pop int value from operand stack.
    ObjectHandler* val2 = actual_frame->popFromStack();
    // Multiply values.
    s4 mul = val1->heap_ptr->_int * val2->heap_ptr->_int;
    // Push result on operand stack.
    ObjectHandler* result = object_heap.createIntHandler(mul);
    actual_frame->pushOnStack(result);
}

void RuntimeEnvironment::iincCase() {
    u1 index = actual_frame->getNext();
    s1 constant = actual_frame->getNext();
    // Get local variable to be incremented.
    ObjectHandler* value = actual_frame->getLocalVariable((u4)index);
    // Increment local variable by constant.
    value->heap_ptr->_int += (u4)constant;
}

void RuntimeEnvironment::iorCase() {
    // Pop int value from operand stack.
    ObjectHandler* val1 = actual_frame->popFromStack();
    // Pop int value from operand stack.
    ObjectHandler* val2 = actual_frame->popFromStack();
    // Count OR of values.
    s4 or_val = val1->heap_ptr->_int | val2->heap_ptr->_int;
    // Push result on operand stack.
    ObjectHandler* result = object_heap.createIntHandler(or_val);
    actual_frame->pushOnStack(result);
}

void RuntimeEnvironment::iandCase() {
    // Pop int value from operand stack.
    ObjectHandler* val1 = actual_frame->popFromStack();
    // Pop int value from operand stack.
    ObjectHandler* val2 = actual_frame->popFromStack();
    // Count AND of values.
    s4 and_val = val1->heap_ptr->_int & val2->heap_ptr->_int;
    // Push result on operand stack.
    ObjectHandler* result = object_heap.createIntHandler(and_val);
    actual_frame->pushOnStack(result);
}

void RuntimeEnvironment::bipushCase() {
    s1 byte = actual_frame->getNext();
    // Create integer value from u1.
    ObjectHandler* value = object_heap.createIntHandler((s4)byte);
    // Push integer value on stack.
    actual_frame->pushOnStack(value);
}

void RuntimeEnvironment::sipushCase() {
    u1 byte1 = actual_frame->getNext();
    u1 byte2 = actual_frame->getNext();
    s2 short_val = (byte1 << 8) | byte2;
    // Create integer value from u2.
    ObjectHandler* value = object_heap.createIntHandler((s4)short_val);
    // Push integer value on stack.
    actual_frame->pushOnStack(value);
}

void RuntimeEnvironment::invokevirtualCase() {
    // Get Methodref index to constant pool.
    u1 indexbyte1 = actual_frame->getNext();
    u1 indexbyte2 = actual_frame->getNext();
    u2 index = (indexbyte1 << 8) | indexbyte2;

    // Get current class definition.
    JavaClass* actual_class = actual_frame->getMethodOwnerClass();
    #ifdef DEBUG
    cout << "InvokeVirtual === Actual class: " << actual_class->getName() << endl;
    #endif
    ConstantPoolItem** constant_pool = actual_class->constant_pool;

    // Get method symbolic reference.
    ConstantPoolItem* indexed_item = constant_pool[index];
    if (indexed_item->tag != CONSTANT_Methodref)
        throw runtime_error("Invoke virtual instruction without a symbolic reference to a method.");
    MethodrefConstantPoolItem* methodref_item = (MethodrefConstantPoolItem*) indexed_item;

    // Get name and type of the method.
    NameAndTypeConstantPoolItem* method_name_type_item = (NameAndTypeConstantPoolItem*) constant_pool[methodref_item->name_and_type_index];
    char* method_name;
    actual_class->getStringFromConstantPool(method_name_type_item->name_index, method_name);
    char* method_descriptor;
    actual_class->getStringFromConstantPool(method_name_type_item->descriptor_index, method_descriptor);

    // Get class index and class reference.
    ClassConstantPoolItem* referenced_class_item = (ClassConstantPoolItem*) constant_pool[methodref_item->class_index];
    char* referenced_class_name;
    actual_class->getStringFromConstantPool(referenced_class_item->name_index, referenced_class_name);
    JavaClass* referenced_class = class_heap.getClass(referenced_class_name);
    #ifdef DEBUG
    cout << "InvokeVirtual === Referenced class: " << referenced_class->getName() << endl;
    #endif

    // Find method in class definition.
    MethodItem* method = referenced_class->getMethod(method_name, method_descriptor);
    ObjectHandler** parameters = new ObjectHandler* [method->method_parameters_cnt];
    for (int i=method->method_parameters_cnt-1; i>=0; --i) {
        parameters[i] = actual_frame->popFromStack();

        #ifdef DEBUG
        cout << "\t Invoke virtual with parameter type: " << (int)parameters[i]->type << endl;
        #endif
    }
    ObjectHandler* stack_object = actual_frame->popFromStack();

    // Get real invoked method.
    #ifdef DEBUG
    cout << "InvokeVirtual === Stacked object: " << stack_object->class_definition->getName() << endl;
    #endif
    method = stack_object->class_definition->getMethod(method_name, method_descriptor, referenced_class, false);
    #ifdef DEBUG
    cout << "InvokeVirtual === Real invoked method descriptor: " << method_descriptor << endl;
    #endif

    if ((method->flags & MAF_NATIVE) == 0) { 
        frame_stack.createFrame(method, stack_object);
        frame_stack.getActualFrame()->setLocalVariable(0, stack_object);
        for (u4 i = 0; i < method->method_parameters_cnt; ++i)
            frame_stack.getActualFrame()->setLocalVariable(i+1, parameters[i]);
    }
    else
        callNative(method_name, parameters, referenced_class, stack_object);

    #ifdef DEBUG
    cout << "Invoke virtual: method [" << method_name << "] on class [" << stack_object->class_definition->getName() << "] with " << method->method_parameters_cnt << " parameter(s)" << endl;
    #endif
}

void RuntimeEnvironment::invokespecialCase() {
    // Get Methodref index to constant pool.
    u1 indexbyte1 = actual_frame->getNext();
    u1 indexbyte2 = actual_frame->getNext();
    u2 index = (indexbyte1 << 8) | indexbyte2;

    // Get current class definition.
    JavaClass* actual_class = actual_frame->getMethodOwnerClass();
    #ifdef DEBUG
    cout << "InvokeSpecial === Actual class: " << actual_class->getName() << endl;
    #endif
    ConstantPoolItem** constant_pool = actual_class->constant_pool;

    // Get method symbolic reference.
    ConstantPoolItem* indexed_item = constant_pool[index];
    if (indexed_item->tag != CONSTANT_Methodref)
        throw runtime_error("Invoke special instruction without a symbolic reference to a method.");
    MethodrefConstantPoolItem* methodref_item = (MethodrefConstantPoolItem*) indexed_item;

    // Get name and type of the method.
    indexed_item = constant_pool[methodref_item->name_and_type_index];
    if (indexed_item->tag != CONSTANT_NameAndType)
        throw runtime_error("Invoke special instruction without a name and type of invoked method.");

    NameAndTypeConstantPoolItem* method_name_type_item = (NameAndTypeConstantPoolItem*) indexed_item;
    char* method_name;
    actual_class->getStringFromConstantPool(method_name_type_item->name_index, method_name);
    char* method_descriptor;
    actual_class->getStringFromConstantPool(method_name_type_item->descriptor_index, method_descriptor);    

    // Get referenced class.
    ClassConstantPoolItem* referenced_class_item = (ClassConstantPoolItem*) constant_pool[methodref_item->class_index];
    char* referenced_class_name;
    actual_class->getStringFromConstantPool(referenced_class_item->name_index, referenced_class_name);
    JavaClass* referenced_class = class_heap.getClass(referenced_class_name);
    #ifdef DEBUG
    cout << "InvokeSpecial === Referenced class: " << referenced_class->getName() << endl;
    #endif

    // Find method in referenced class definition.
    #ifdef DEBUG
    cout << "InvokeSpecial === Referenced method descriptor: " << method_descriptor << endl;
    #endif
    MethodItem* method = referenced_class->getMethod(method_name, method_descriptor);
    ObjectHandler** parameters = new ObjectHandler* [method->method_parameters_cnt];
    for (int i=method->method_parameters_cnt-1; i>=0; --i) {
        parameters[i] = actual_frame->popFromStack();

        #ifdef DEBUG
        cout << "\t Invoke special with parameter type: " << (int)parameters[i]->type << endl;
        #endif
    }
    ObjectHandler* stack_object = actual_frame->popFromStack();

    // Get real invoked method.
    #ifdef DEBUG
    cout << "InvokeSpecial === Stacked object: " << stack_object->class_definition->getName() << endl;
    #endif
    method = stack_object->class_definition->getMethod(method_name, method_descriptor, referenced_class, true);
    #ifdef DEBUG
    cout << "InvokeSpecial === Real invoked method descriptor: " << method_descriptor << endl;
    #endif

    if ((method->flags & MAF_NATIVE) == 0) { 
        frame_stack.createFrame(method, stack_object);
        frame_stack.getActualFrame()->setLocalVariable(0, stack_object);
        for (u4 i = 0; i < method->method_parameters_cnt; ++i)
            frame_stack.getActualFrame()->setLocalVariable(i+1, parameters[i]);
    }
    else
        callNative(method_name, parameters, referenced_class, stack_object);

    #ifdef DEBUG
    cout << "Invoke special: method [" << method_name << "] on class [" << stack_object->class_definition->getName() << "] with " << method->method_parameters_cnt << " parameter(s)" << endl;
    #endif
}

void RuntimeEnvironment::ifCase(u1 operation) {
    int value = actual_frame->popFromStack()->heap_ptr->_int;
    bool result;

    switch (operation) {
        case ifeq:
            result = value == 0;
            break;
        case ifne:
            result = value != 0;
            break;
        case iflt:
            result = value < 0;
            break;
        case ifge:
            result = value >= 0;
            break;
        case ifgt:
            result = value > 0;
            break;
        case ifle:
            result = value <= 0;
            break;
        default:
            throw runtime_error("Not an if<cond> operation!");
    }

    if (result == true) {
        u1 branchbyte1 = actual_frame->getNext();
        u1 branchbyte2 = actual_frame->getNext();
        s2 offset = (branchbyte1 << 8) | branchbyte2;
        offset -= 3; // Execution then proceeds at that offset from the address of the opcode of this if_icmp<cond> instruction. 2 branch bytes were already readed.
        actual_frame->pc += offset;
    }
    else
        actual_frame->pc += 2; // Skip two branch bytes.
}

void RuntimeEnvironment::if_icmpCase(u1 operation) {
    int value2 = actual_frame->popFromStack()->heap_ptr->_int;
    int value1 = actual_frame->popFromStack()->heap_ptr->_int;
    bool result;

    switch (operation) {
        case if_icmpeq:
            result = value1 == value2;
            break;
        case if_icmpne:
            result = value1 != value2;
            break;
        case if_icmplt:
            result = value1 < value2;
            break;
        case if_icmpge:
            result = value1 >= value2;
            break;
        case if_icmpgt:
            result = value1 > value2;
            break;
        case if_icmple:
            result = value1 <= value2;
            break;
        default:
            throw runtime_error("Not an if_icmp<cond> operation!");
    }

    if (result == true) {
        u1 branchbyte1 = actual_frame->getNext();
        u1 branchbyte2 = actual_frame->getNext();
        s2 offset = (branchbyte1 << 8) | branchbyte2;
        offset -= 3; // Execution then proceeds at that offset from the address of the opcode of this if_icmp<cond> instruction. 2 branch bytes were already readed.
        actual_frame->pc += offset;
    }
    else
        actual_frame->pc += 2; // Skip two branch bytes.
}

void RuntimeEnvironment::ldcCase() {
    u1 index = actual_frame->getNext();

    // Get current class definition.
    JavaClass* class_definition = actual_frame->getMethodOwnerClass();
    ConstantPoolItem* item = class_definition->constant_pool[index];

    ObjectHandler* value;
    switch (item->tag) {
        case CONSTANT_Integer:
            value = object_heap.createIntHandler((s4)((IntegerConstantPoolItem*) item)->bytes);
            break;
        case CONSTANT_String:
            value = object_heap.createStringHandler(class_definition, ((StringConstantPoolItem*) item)->string_index);
            break;
        default:
            printf("LDC instruction hasn't implentation for constant-tag: %d\n", (int)(item->tag));
            throw runtime_error("Requested operation inside LDC instruction is not implementated.");
    }

    value->class_definition = class_definition; // TODO Is it necessary?
    actual_frame->pushOnStack(value);
}

void RuntimeEnvironment::clinit(JavaClass* javaclass) {
    if (javaclass->hasMethod("<clinit>") == false) {
        string message("Class ");
        message.append(javaclass->getName()).append(" with static field hasn't <clinit>.");
        throw runtime_error(message);
    }

    MethodItem* clinit = javaclass->getMethod("<clinit>");
    #ifdef DEBUG
        cout << "Running clinit of class: " << javaclass->getName() << endl;
    #endif

    frame_stack.createFrame(clinit, object_heap.createStaticHandler(javaclass));
    javaclass->initialized = true;
}

void RuntimeEnvironment::callNative(char* called_method_name, ObjectHandler** parameters, JavaClass* javaclass, ObjectHandler* referenced_object) {
    if ((strcmp(called_method_name, "println") == 0 || strcmp(called_method_name, "print") == 0) && javaclass->getName() == "java/io/PrintStream") {
        ObjectHandler* value = parameters[0];
        if (value->type == FIELD_STRING) {
            u2 string_index = value->heap_ptr->_short;

            char* println_string;
            actual_frame->getMethodOwnerClass()->getStringFromConstantPool(string_index, println_string);
            printf("%s", println_string);
        }
        else if (value->type == FIELD_INT)
            printf("%d", value->heap_ptr->_int);
        else if (value->type == FIELD_CHAR)
            printf("%c", (char) value->heap_ptr->_char);
        else
            throw runtime_error("Native method 'println' called with non-string argument.");

        if (strcmp(called_method_name, "println") == 0)
            printf("\n");
    }
    else if (strcmp(called_method_name, "read") == 0) {
        if (javaclass->getName() == "java/io/InputStream") {
            char scanf_char;
            scanf(" %c", &scanf_char);

            actual_frame->pushOnStack(object_heap.createIntHandler((s4) scanf_char));
        }
        else if (javaclass->getName() == "java/io/FileReader") {
            // The first field is a File object.
            ObjectHandler* file_object_handler = (ObjectHandler*)((referenced_object->heap_ptr + 1)->_ptr);

            // The first field is the file name.
            ObjectHandler* file_name_object_handler = (ObjectHandler*)((file_object_handler->heap_ptr + 1)->_ptr);
            char* file_name;
            file_name_object_handler->class_definition->getStringFromConstantPool(file_name_object_handler->heap_ptr->_short, file_name);

            FILE* file_pointer = open_files.getFile(file_object_handler, file_name);
            int char_value = fgetc(file_pointer);
            actual_frame->pushOnStack(object_heap.createIntHandler((s4) char_value));
        }
        else
            goto error;
    }
    else {
        error:
        string message = "Unknown native method: ";
        message.append(called_method_name).append(" called on object: ").append(javaclass->getName());
        throw runtime_error(message);
    }
}

#ifdef STACK_DUMP
void RuntimeEnvironment::stack_dump() {
    printf("\t\t\t\t\t\t\tActual frame STACK DUMP\n");

    ObjectHandler* object_handler;
    for (u4 i = 0; i < actual_frame->getStackSize(); ++i) {
        object_handler = actual_frame->getStackItem(i);

        if (object_handler == NULL) {
            printf(" | NULL [!]\n");
            continue;
        }

        printf("\t\t\t\t\t\t\tType: %d", object_handler->type);
        if (object_handler->type == FIELD_CLASS)
            printf(" | CLASS | %s\n", object_handler->class_definition->getName().c_str());
        else if (object_handler->type == FIELD_STRING) {
            char* string_value;
            object_handler->class_definition->getStringFromConstantPool(object_handler->heap_ptr->_short, string_value);
            printf(" | STRING | %s\n", string_value);
        }
        else if (object_handler->type == FIELD_INT)
            printf(" | INTEGER | %d\n", object_handler->heap_ptr->_int);
        else if (object_handler->type == FIELD_CHAR)
            printf(" | CHAR | %c (INT: %d)\n", (char) object_handler->heap_ptr->_char, (int) object_handler->heap_ptr->_char);
        else
           printf("\n");
    }
    printf("\t\t\t\t\t\t\t--- --- --- ---\n");
}
#endif