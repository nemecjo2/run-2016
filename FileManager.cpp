#include "FileManager.h"

#include <stdexcept>

FileManager::FileManager() {
}

FileManager::~FileManager() {
    map<ObjectHandler*, FILE*>::iterator it = open_files.begin();
    for (; it != open_files.end(); ++it)
        fclose(it->second);
}

FILE* FileManager::getFile(ObjectHandler* file_instance, char* file_name) {
    map<ObjectHandler*, FILE*>::iterator it = open_files.find(file_instance);
    if (it != open_files.end())
        return it->second;
    else {
        #ifdef DEBUG
        printf("Open file: %s\n", file_name);
        #endif

        FILE* file_pointer = fopen(file_name, "rb");
        if (file_pointer == NULL)
            throw runtime_error("Couldn't open file.");

        open_files.insert(pair<ObjectHandler*, FILE*>(file_instance, file_pointer));
        return file_pointer;
    }
}