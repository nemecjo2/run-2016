#include <cstdlib>
#include <iostream>
#include "TestEnv/TestEnv.h"
#include "JavaClass.h"
#include "Constants.h"
#include "JavaClassPrinter.h"

void JavaClassPrinter::printJavaClass() {
    cout << "Minor version: " << javaclass->minor_version << endl;
    cout << "Major version: " << javaclass->major_version << endl;

    cout << "Constant pool count: " << javaclass->constant_pool_count-1 << endl;
    for (int i=1; i<javaclass->constant_pool_count; ++i) {
        cout << "  #" << i << " = ";
        printConstantPoolItem(i);
    }
    cout << endl;

    cout << "Access flags: ";
    printClassAccessFlags(javaclass->access_flags);
    cout << endl;
    cout << "This class ref: #" << javaclass->this_class << endl;
    cout << "Super class ref: #" << javaclass->super_class << endl;
    cout << "Interfaces count: " << javaclass->interfaces_count << endl;

    cout << "Fields count: " << javaclass->fields_count << endl;
    for (int i=0; i<javaclass->fields_count; ++i) {
        cout << "  #" << i << " = ";
        printFieldItem(javaclass->fields[i]);
    }
    cout << endl;

    cout << "Methods count: " << javaclass->methods_count << endl;
    for (int i=0; i<javaclass->methods_count; ++i) {
        cout << "  #" << i << " = ";
        printMethodItem(javaclass->methods[i]);
    }
    cout << endl;

    cout << "Attributes count: " << javaclass->attributes_count << endl;
    for (int i=0; i<javaclass->attributes_count; ++i) {
        cout << "  #" << i << " = ";
        printAttributeItem(javaclass->attributes[i]);
    }
    cout << endl;
}

void JavaClassPrinter::printFieldItem(FieldItem* item) {
    char * fieldName;
    if (javaclass->getStringFromConstantPool(item->name_index, fieldName))
        cout << fieldName << ", ";

    cout << "attrs:" << item->attributes_count << endl;
    cout << "\taccess: ";
    printMethodAccessFlags(item->flags);
    cout << endl;
}

void JavaClassPrinter::printMethodItem(MethodItem* item) {
    char * fieldName;
    if (javaclass->getStringFromConstantPool(item->name_index, fieldName))
        cout << fieldName << ", ";

    cout << "attrs:" << item->attributes_count << endl;
    cout << "\taccess: ";
    printMethodAccessFlags(item->flags);
    cout << endl;
}

void JavaClassPrinter::printAttributeItem(AttributeItem* item) {
    char * fieldName;
    if (javaclass->getStringFromConstantPool(item->name_index, fieldName))
        cout << fieldName << ", length:" << item->length << endl;
}

void JavaClassPrinter::printConstantPoolItem(int i) {
    GeneralConstantPoolItem* item = (GeneralConstantPoolItem*)javaclass->constant_pool[i];
    switch(item->tag) {
        case CONSTANT_Utf8:
            char * string;
            javaclass->getStringFromConstantPool(i, string);
            cout << "Utf8\t" << string << endl;
            break;
        case CONSTANT_Integer:
            cout << "Integer" << endl;
            break;
        case CONSTANT_Float:
            cout << "Float" << endl;
            break;
        case CONSTANT_Long:
            cout << "Long" << endl;
            break;
        case CONSTANT_Double:
            cout << "Double" << endl;
            break;
        case CONSTANT_Class:
            cout << "Class\t#" << ((ClassConstantPoolItem*)item)->name_index << endl;
            break;
        case CONSTANT_String:
            cout << "String" << endl;
            break;
        case CONSTANT_Fieldref:
            cout << "Field\t#" << ((FieldrefConstantPoolItem*) item)->class_index;
            cout << ".#" << ((FieldrefConstantPoolItem*) item)->name_and_type_index << endl;
            break;
        case CONSTANT_Methodref:
            cout << "Method\t#" << ((MethodrefConstantPoolItem*) item)->class_index;
            cout << ".#" << ((MethodrefConstantPoolItem*) item)->name_and_type_index << endl;
            break;
        case CONSTANT_InterfaceMethodref:
            cout << "Interface" << endl;
            break;
        case CONSTANT_NameAndType:
            cout << "Nm Tp\t#" << ((NameAndTypeConstantPoolItem*) item)->name_index;
            cout << ".#" << ((NameAndTypeConstantPoolItem*) item)->descriptor_index << endl;
            break;
    }
}

void JavaClassPrinter::printClassAccessFlags(u2 flags) {
    if (flags & CAF_PUBLIC) {
        cout << "Public, ";
    }
    if (flags & CAF_ABSTRACT) {
        cout << "Abstract, ";
    }
    if (flags & CAF_INTERFACE) {
        cout << "Interface, ";
    }
    if (flags & CAF_FINAL) {
        cout << "Final, ";
    }
    if (flags & CAF_SUPER) {
        cout << "Super, ";
    }
}

void JavaClassPrinter::printMethodAccessFlags(u2 flags) {
    if (flags & MAF_PUBLIC) {
        cout << "Public, ";
    }
    if (flags & MAF_PRIVATE) {
        cout << "Private, ";
    }
    if (flags & MAF_PROTECTED) {
        cout << "Protected, ";
    }
    if (flags & MAF_ABSTRACT) {
        cout << "Abstract, ";
    }
    if (flags & MAF_STATIC) {
        cout << "Static, ";
    }
    if (flags & MAF_FINAL) {
        cout << "Final, ";
    }
    if (flags & MAF_SYNCHRONIZED) {
        cout << "Synchronized, ";
    }
    if (flags & MAF_NATIVE) {
        cout << "Native, ";
    }
    if (flags & MAF_ABSTRACT) {
        cout << "Abstract, ";
    }
    if (flags & MAF_STRICT) {
        cout << "Strict, ";
    }
}