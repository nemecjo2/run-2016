#ifndef OBJECTHEAP_H
#define OBJECTHEAP_H

#include "DataTypes.h"
#include "FrameStack.h"
#include "GarbageCollector.h"
#include "FileManager.h"

class ObjectHeap {
    public:
        /**
         * Creates Object heap with given size.
         * @param heap_size Size of Object heap.
         */
        ObjectHeap(u4, FrameStack*, ClassHeap*, FileManager*);
       ~ObjectHeap();

       /**
        * Creates Variable on Object Heap for given type.
        * @param Integer value.
        * @return ObjectHandler* Returns pointer for created ObjectHandler.
        */
       ObjectHandler* createIntHandler(s4);

       /**
        * Creates Variable on Object Heap for given type.
        * @param Boolean value.
        * @return ObjectHandler* Returns pointer for created ObjectHandler.
        */
       ObjectHandler* createBoolHandler(u1);

       /**
        * Creates Variable on Object Heap for String type.
        * @param u2 string_index.
        * @return ObjectHandler* Returns pointer for created ObjectHandler.
        */
       ObjectHandler* createStringHandler(JavaClass*, u2);

       /**
        * Creates object on Object Heap for given Class.
        * @param JavaClass* Pointer to JavaClass to be instantiated.
        * @return ObjectHandler* Returns pointer for created ObjectHandler.
        */
       ObjectHandler* createObjectHandler(JavaClass*);

        /**
        * Creates static Object handler on Object Heap for given Class.
        * @param JavaClass* Pointer to JavaClass.
        * @return ObjectHandler* Returns pointer for created Handler.
        */
       ObjectHandler* createStaticHandler(JavaClass*);

       /**
        * Creates array on Object Heap for given size and type.
        * @param u4 Array size.
        * @param u1 Array type.
        * @return ObjectHandler* Returns pointer for created ObjectHandler.
        */
       ObjectHandler* createArrayHandler(u4, u1);

       /**
        * Creates array on Object Heap for given size and type.
        * @param Array size.
        * @param Array type (Class).
        * @return ObjectHandler* Returns pointer for created ObjectHandler.
        */
       ObjectHandler* createArrayHandler(u4, JavaClass*);

       /**
        * Creates one empty Object Handler.
        * @return ObjectHandler* Returns pointer for created ObjectHandler.
        */
       ObjectHandler* createEmptyHandler();

    private:
        u4 heap_size;
        Variable* memory;
        bool* memory_usages;
        Variable* new_object_ptr;
        GarbageCollector* garbage_collector;

        /**
         * Allocates free space for specified size in memory.
         * @param size Requested size.
         * @return Variable* Pointer to allocated memory.
         * @throw runtime_error Throws if not enough space in memory.
         */
        Variable* getFreeSpace(u2);

        /**
         * Looking for free block of size given in parameter.
         * @param size Requested size.
         * @return u4 index into free memory. u4 < heap_size if free block exists, u4 == heap_size otherwise.
         */        
        u4 _getFreeSpace(u2);

        #ifdef MEMORY_DUMP
        FrameStack* frame_stack;
        void memoryDump();
        #endif
};

#endif