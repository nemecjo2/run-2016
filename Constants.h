#ifndef CONSTANTS_H
#define CONSTANTS_H

// DEBUG SWITCH
//#define DEBUG
// MEMORY DUMP SWITCH
//#define MEMORY_DUMP
// STACK DUMP SWITCH
//#define STACK_DUMP

// ================================== JVM ==================================

#define CONSTANT_Utf8               1
#define CONSTANT_Integer            3
#define CONSTANT_Float              4
#define CONSTANT_Long               5
#define CONSTANT_Double             6
#define CONSTANT_Class              7
#define CONSTANT_String             8
#define CONSTANT_Fieldref           9
#define CONSTANT_Methodref          10
#define CONSTANT_InterfaceMethodref 11
#define CONSTANT_NameAndType        12
#define CONSTANT_MethodHandle       15
#define CONSTANT_MethodType         16
#define CONSTANT_InvokeDynamic      18

#define FIELD_ARRAY                 '['
#define FIELD_BOOL                  'Z'
#define FIELD_CHAR                  'C'
#define FIELD_FLOAT                 'F'
#define FIELD_DOUBLE                'D'
#define FIELD_BYTE                  'B'
#define FIELD_SHORT                 'S'
#define FIELD_INT                   'I'
#define FIELD_LONG                  'J'
#define FIELD_CLASS                 'L'
#define FIELD_STRING                'S' // This is a custom flag.

/* NM = Name Index */
#define NM_CODE "Code"
#define NM_CONSTANT_VALUE "ConstantValue"

/* CAF = Class Access Flag */
#define CAF_PUBLIC          0x0001 /*Declared public; may be accessed from outside its package.  */
#define CAF_FINAL           0x0010 /*Declared final; no subclasses allowed.  */
#define CAF_SUPER           0x0020 /*Treat superclass methods specially when invoked by the invokespecial instruction.  */
#define CAF_INTERFACE       0x0200 /*Is an interface, not a class.  */
#define CAF_ABSTRACT        0x0400 /*Declared abstract; may not be instantiated.  */

/* MAF = Method Access Flag */
#define MAF_PUBLIC          0x0001  /*Declared public; may be accessed from outside its package.  */
#define MAF_PRIVATE         0x0002  /*Declared private; accessible only within the defining class.  */
#define MAF_PROTECTED       0x0004  /*Declared protected; may be accessed within subclasses.  */
#define MAF_STATIC          0x0008  /*Declared static.  */
#define MAF_FINAL           0x0010  /*Declared final; may not be overridden.  */
#define MAF_SYNCHRONIZED    0x0020  /*Declared synchronized; invocation is wrapped in a monitor lock.  */
#define MAF_NATIVE          0x0100  /*Declared native; implemented in a language other than Java.  */
#define MAF_ABSTRACT        0x0400  /*Declared abstract; no implementation is provided.  */
#define MAF_STRICT          0x0800  /*Declared strictfp; floating-point mode is FP-strict  */

// ================================== OWN ==================================

/* ATTR = Attribute */
const int ATTR_BASIC =          1;
const int ATTR_CONSTANT_VALUE = 2;
const int ATTR_CODE =           3;

/* Sizes */
const int EXCEPTION_ITEM_SIZE = 8;
const int DEFAULT_HEAP_SIZE = 1000;

#endif